<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Validation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgAllPerf = New System.Windows.Forms.DataGridView
        Me.cbxAcct = New System.Windows.Forms.ComboBox
        Me.btnAll = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        CType(Me.dgAllPerf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgAllPerf
        '
        Me.dgAllPerf.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAllPerf.BackgroundColor = System.Drawing.Color.White
        Me.dgAllPerf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgAllPerf.Location = New System.Drawing.Point(12, 64)
        Me.dgAllPerf.Name = "dgAllPerf"
        Me.dgAllPerf.Size = New System.Drawing.Size(532, 474)
        Me.dgAllPerf.TabIndex = 0
        '
        'cbxAcct
        '
        Me.cbxAcct.FormattingEnabled = True
        Me.cbxAcct.Location = New System.Drawing.Point(27, 22)
        Me.cbxAcct.Name = "cbxAcct"
        Me.cbxAcct.Size = New System.Drawing.Size(240, 21)
        Me.cbxAcct.TabIndex = 1
        Me.cbxAcct.Text = "Select Account/Group"
        '
        'btnAll
        '
        Me.btnAll.Location = New System.Drawing.Point(283, 21)
        Me.btnAll.Name = "btnAll"
        Me.btnAll.Size = New System.Drawing.Size(75, 23)
        Me.btnAll.TabIndex = 2
        Me.btnAll.Text = "View All"
        Me.btnAll.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnAll)
        Me.Panel1.Controls.Add(Me.cbxAcct)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(938, 58)
        Me.Panel1.TabIndex = 3
        '
        'Validation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(559, 550)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.dgAllPerf)
        Me.Name = "Validation"
        Me.Text = "Validation"
        CType(Me.dgAllPerf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgAllPerf As System.Windows.Forms.DataGridView
    Friend WithEvents cbxAcct As System.Windows.Forms.ComboBox
    Friend WithEvents btnAll As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
