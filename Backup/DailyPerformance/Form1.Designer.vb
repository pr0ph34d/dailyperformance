<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFolderPath = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtRepFolder = New System.Windows.Forms.TextBox
        Me.btnRunRep = New System.Windows.Forms.Button
        Me.fbdFolder = New System.Windows.Forms.FolderBrowserDialog
        Me.ofdOpen = New System.Windows.Forms.OpenFileDialog
        Me.lblDone = New System.Windows.Forms.Label
        Me.txtProgress = New System.Windows.Forms.TextBox
        Me.lblRunDate = New System.Windows.Forms.Label
        Me.lnkLog = New System.Windows.Forms.LinkLabel
        Me.btnFind = New System.Windows.Forms.Button
        Me.btnEmail = New System.Windows.Forms.Button
        Me.dtpRunDate = New System.Windows.Forms.DateTimePicker
        Me.cbxDaily = New System.Windows.Forms.CheckBox
        Me.cbxBB = New System.Windows.Forms.CheckBox
        Me.cbxBob = New System.Windows.Forms.CheckBox
        Me.cbx130 = New System.Windows.Forms.CheckBox
        Me.txtCustomMsg = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbxTest = New System.Windows.Forms.CheckBox
        Me.btnEmailDM = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtDivs = New System.Windows.Forms.TextBox
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuUpdateAccount = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuValidation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuValidPerf = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuIndex = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAdmin = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuStartDates = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDisplay = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdDisplay = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuOpenFile = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblFolderPath
        '
        Me.lblFolderPath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblFolderPath.Location = New System.Drawing.Point(12, 65)
        Me.lblFolderPath.Name = "lblFolderPath"
        Me.lblFolderPath.Size = New System.Drawing.Size(225, 19)
        Me.lblFolderPath.TabIndex = 45
        Me.lblFolderPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 19)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "Report Folder:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRepFolder
        '
        Me.txtRepFolder.Location = New System.Drawing.Point(108, 37)
        Me.txtRepFolder.Name = "txtRepFolder"
        Me.txtRepFolder.Size = New System.Drawing.Size(100, 20)
        Me.txtRepFolder.TabIndex = 0
        Me.txtRepFolder.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnRunRep
        '
        Me.btnRunRep.Location = New System.Drawing.Point(252, 37)
        Me.btnRunRep.Name = "btnRunRep"
        Me.btnRunRep.Size = New System.Drawing.Size(167, 45)
        Me.btnRunRep.TabIndex = 2
        Me.btnRunRep.Text = "Run Reports"
        Me.btnRunRep.UseVisualStyleBackColor = True
        '
        'lblDone
        '
        Me.lblDone.Location = New System.Drawing.Point(279, 92)
        Me.lblDone.Name = "lblDone"
        Me.lblDone.Size = New System.Drawing.Size(117, 23)
        Me.lblDone.TabIndex = 97
        Me.lblDone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtProgress
        '
        Me.txtProgress.AcceptsReturn = True
        Me.txtProgress.AcceptsTab = True
        Me.txtProgress.Location = New System.Drawing.Point(435, 37)
        Me.txtProgress.Multiline = True
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtProgress.Size = New System.Drawing.Size(344, 319)
        Me.txtProgress.TabIndex = 3
        '
        'lblRunDate
        '
        Me.lblRunDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRunDate.Location = New System.Drawing.Point(12, 115)
        Me.lblRunDate.Name = "lblRunDate"
        Me.lblRunDate.Size = New System.Drawing.Size(223, 22)
        Me.lblRunDate.TabIndex = 100
        Me.lblRunDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lnkLog
        '
        Me.lnkLog.AutoSize = True
        Me.lnkLog.Location = New System.Drawing.Point(15, 340)
        Me.lnkLog.Name = "lnkLog"
        Me.lnkLog.Size = New System.Drawing.Size(55, 13)
        Me.lnkLog.TabIndex = 4
        Me.lnkLog.TabStop = True
        Me.lnkLog.Text = "View log"
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(214, 37)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(22, 23)
        Me.btnFind.TabIndex = 1
        Me.btnFind.Text = ">"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'btnEmail
        '
        Me.btnEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnEmail.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnEmail.Location = New System.Drawing.Point(317, 292)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(83, 33)
        Me.btnEmail.TabIndex = 3
        Me.btnEmail.Text = "Send emails"
        Me.btnEmail.UseVisualStyleBackColor = True
        '
        'dtpRunDate
        '
        Me.dtpRunDate.Location = New System.Drawing.Point(12, 146)
        Me.dtpRunDate.Name = "dtpRunDate"
        Me.dtpRunDate.Size = New System.Drawing.Size(224, 20)
        Me.dtpRunDate.TabIndex = 112
        '
        'cbxDaily
        '
        Me.cbxDaily.AutoSize = True
        Me.cbxDaily.Checked = True
        Me.cbxDaily.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxDaily.Location = New System.Drawing.Point(231, 213)
        Me.cbxDaily.Name = "cbxDaily"
        Me.cbxDaily.Size = New System.Drawing.Size(81, 17)
        Me.cbxDaily.TabIndex = 113
        Me.cbxDaily.Text = "Daily Perf"
        Me.cbxDaily.UseVisualStyleBackColor = True
        '
        'cbxBB
        '
        Me.cbxBB.AutoSize = True
        Me.cbxBB.Checked = True
        Me.cbxBB.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxBB.Location = New System.Drawing.Point(231, 247)
        Me.cbxBB.Name = "cbxBB"
        Me.cbxBB.Size = New System.Drawing.Size(86, 17)
        Me.cbxBB.TabIndex = 114
        Me.cbxBB.Text = "Blackberry"
        Me.cbxBB.UseVisualStyleBackColor = True
        '
        'cbxBob
        '
        Me.cbxBob.AutoSize = True
        Me.cbxBob.Checked = True
        Me.cbxBob.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxBob.Location = New System.Drawing.Point(335, 213)
        Me.cbxBob.Name = "cbxBob"
        Me.cbxBob.Size = New System.Drawing.Size(48, 17)
        Me.cbxBob.TabIndex = 115
        Me.cbxBob.Text = "Bob"
        Me.cbxBob.UseVisualStyleBackColor = True
        '
        'cbx130
        '
        Me.cbx130.AutoSize = True
        Me.cbx130.Checked = True
        Me.cbx130.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbx130.Location = New System.Drawing.Point(335, 247)
        Me.cbx130.Name = "cbx130"
        Me.cbx130.Size = New System.Drawing.Size(65, 17)
        Me.cbx130.TabIndex = 116
        Me.cbx130.Text = "130-30"
        Me.cbx130.UseVisualStyleBackColor = True
        '
        'txtCustomMsg
        '
        Me.txtCustomMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomMsg.Location = New System.Drawing.Point(12, 213)
        Me.txtCustomMsg.Multiline = True
        Me.txtCustomMsg.Name = "txtCustomMsg"
        Me.txtCustomMsg.Size = New System.Drawing.Size(195, 112)
        Me.txtCustomMsg.TabIndex = 117
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 188)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(193, 22)
        Me.Label2.TabIndex = 118
        Me.Label2.Text = "Add custom message:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTest
        '
        Me.cbxTest.AutoSize = True
        Me.cbxTest.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbxTest.Location = New System.Drawing.Point(231, 301)
        Me.cbxTest.Name = "cbxTest"
        Me.cbxTest.Size = New System.Drawing.Size(78, 17)
        Me.cbxTest.TabIndex = 119
        Me.cbxTest.Text = "Test only"
        Me.cbxTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbxTest.UseVisualStyleBackColor = True
        '
        'btnEmailDM
        '
        Me.btnEmailDM.Location = New System.Drawing.Point(252, 124)
        Me.btnEmailDM.Name = "btnEmailDM"
        Me.btnEmailDM.Size = New System.Drawing.Size(167, 45)
        Me.btnEmailDM.TabIndex = 120
        Me.btnEmailDM.Text = "Email Data Mgmt"
        Me.btnEmailDM.UseVisualStyleBackColor = True
        Me.btnEmailDM.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1368, 510)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 128
        Me.Label3.Text = "Label3"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 97)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 13)
        Me.Label5.TabIndex = 137
        Me.Label5.Text = "Dividends Posted:"
        '
        'txtDivs
        '
        Me.txtDivs.Location = New System.Drawing.Point(137, 93)
        Me.txtDivs.Name = "txtDivs"
        Me.txtDivs.Size = New System.Drawing.Size(100, 20)
        Me.txtDivs.TabIndex = 136
        Me.txtDivs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUpdateAccount, Me.mnuValidation, Me.mnuAdmin, Me.mnuAbout})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(800, 24)
        Me.MenuStrip1.TabIndex = 140
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuUpdateAccount
        '
        Me.mnuUpdateAccount.Name = "mnuUpdateAccount"
        Me.mnuUpdateAccount.Size = New System.Drawing.Size(117, 20)
        Me.mnuUpdateAccount.Text = "Update Performance"
        '
        'mnuValidation
        '
        Me.mnuValidation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuValidPerf, Me.mnuIndex})
        Me.mnuValidation.Name = "mnuValidation"
        Me.mnuValidation.Size = New System.Drawing.Size(65, 20)
        Me.mnuValidation.Text = "Validation"
        '
        'mnuValidPerf
        '
        Me.mnuValidPerf.Name = "mnuValidPerf"
        Me.mnuValidPerf.Size = New System.Drawing.Size(137, 22)
        Me.mnuValidPerf.Text = "Performance"
        '
        'mnuIndex
        '
        Me.mnuIndex.Name = "mnuIndex"
        Me.mnuIndex.Size = New System.Drawing.Size(137, 22)
        Me.mnuIndex.Text = "Index Prices"
        '
        'mnuAdmin
        '
        Me.mnuAdmin.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuStartDates, Me.mnuDisplay})
        Me.mnuAdmin.Name = "mnuAdmin"
        Me.mnuAdmin.Size = New System.Drawing.Size(48, 20)
        Me.mnuAdmin.Text = "Admin"
        '
        'mnuStartDates
        '
        Me.mnuStartDates.Name = "mnuStartDates"
        Me.mnuStartDates.Size = New System.Drawing.Size(168, 22)
        Me.mnuStartDates.Text = "Custom Start Dates"
        '
        'mnuDisplay
        '
        Me.mnuDisplay.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUpdDisplay, Me.mnuOpenFile})
        Me.mnuDisplay.Name = "mnuDisplay"
        Me.mnuDisplay.Size = New System.Drawing.Size(168, 22)
        Me.mnuDisplay.Text = "Output Display"
        '
        'mnuUpdDisplay
        '
        Me.mnuUpdDisplay.Name = "mnuUpdDisplay"
        Me.mnuUpdDisplay.Size = New System.Drawing.Size(152, 22)
        Me.mnuUpdDisplay.Text = "Update"
        '
        'mnuOpenFile
        '
        Me.mnuOpenFile.Name = "mnuOpenFile"
        Me.mnuOpenFile.Size = New System.Drawing.Size(152, 22)
        Me.mnuOpenFile.Text = "Open File"
        '
        'mnuAbout
        '
        Me.mnuAbout.Name = "mnuAbout"
        Me.mnuAbout.Size = New System.Drawing.Size(47, 20)
        Me.mnuAbout.Text = "About"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DodgerBlue
        Me.ClientSize = New System.Drawing.Size(800, 374)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDivs)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnEmailDM)
        Me.Controls.Add(Me.cbxTest)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtCustomMsg)
        Me.Controls.Add(Me.cbx130)
        Me.Controls.Add(Me.cbxBob)
        Me.Controls.Add(Me.cbxBB)
        Me.Controls.Add(Me.cbxDaily)
        Me.Controls.Add(Me.dtpRunDate)
        Me.Controls.Add(Me.btnEmail)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.lnkLog)
        Me.Controls.Add(Me.lblRunDate)
        Me.Controls.Add(Me.txtProgress)
        Me.Controls.Add(Me.btnRunRep)
        Me.Controls.Add(Me.lblDone)
        Me.Controls.Add(Me.lblFolderPath)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtRepFolder)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "Form1"
        Me.Text = "Daily Performance"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblFolderPath As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtRepFolder As System.Windows.Forms.TextBox
    Friend WithEvents btnRunRep As System.Windows.Forms.Button
    Friend WithEvents fbdFolder As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ofdOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblDone As System.Windows.Forms.Label
    Friend WithEvents txtProgress As System.Windows.Forms.TextBox
    Friend WithEvents lblRunDate As System.Windows.Forms.Label
    Friend WithEvents lnkLog As System.Windows.Forms.LinkLabel
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents btnEmail As System.Windows.Forms.Button
    Friend WithEvents dtpRunDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbxDaily As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBB As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBob As System.Windows.Forms.CheckBox
    Friend WithEvents cbx130 As System.Windows.Forms.CheckBox
    Friend WithEvents txtCustomMsg As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxTest As System.Windows.Forms.CheckBox
    Friend WithEvents btnEmailDM As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDivs As System.Windows.Forms.TextBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuUpdateAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuValidation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuValidPerf As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuIndex As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAdmin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStartDates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpdDisplay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOpenFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.ToolStripMenuItem

End Class
