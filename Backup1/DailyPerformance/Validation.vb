Public Class Validation

#Region "Variables"

    Dim getdata As DataAccess = New DataAccess
    Dim dtPerf As DataTable = New DataTable
    Dim dsPerf As DataSet = New DataSet
    Dim rdate As Date

    Public Property rundate() As Date
        Get
            Return rdate
        End Get
        Set(ByVal Value As Date)
            rdate = Value
        End Set
    End Property

    Public Sub New(ByVal rdate As Date)
        MyBase.New()
        InitializeComponent()
        rundate = rdate
    End Sub

#End Region
    Dim connPM As String

    Public Property ConnectionPM() As String
        Get
            Return connPM
        End Get
        Set(ByVal Value As String)
            connPM = Value
        End Set
    End Property

    Private Sub Validation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ViewAll()

        dsPerf = getdata.GetPerformance(connPM, rdate, "all")
        Dim p As Integer
        For p = 0 To dsPerf.Tables.Count - 1
            cbxAcct.Items.Add(dsPerf.Tables(p).Rows(0).Item("Account"))
        Next
    End Sub

    Private Sub cbxAcct_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxAcct.SelectionChangeCommitted
        dgAllPerf.DataSource = dsPerf.Tables(Me.cbxAcct.SelectedIndex)
    End Sub

    Private Sub btnAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll.Click
        ViewAll()
    End Sub

    Private Sub ViewAll()
        dtPerf = getdata.GetAllPerf(connPM, rdate)
        dgAllPerf.DataSource = dtPerf

        dgAllPerf.Columns(0).Width = 200
        dgAllPerf.Columns(1).Width = 60
        dgAllPerf.Columns(2).Width = 50
        dgAllPerf.Columns(3).Width = 50
        dgAllPerf.Columns(4).Width = 50
        dgAllPerf.Columns(5).Width = 50

        Me.cbxAcct.Text = "Select Account/Group"
    End Sub
End Class