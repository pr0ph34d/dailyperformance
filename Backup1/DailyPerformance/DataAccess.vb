Public Class DataAccess
    Dim datafilepath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\DataFiles\"
    Dim reppath As String = ""


    Public Function GetPRFMGroups(ByVal connPort As String)

        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_allPRFMgroups", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 50000


        Dim DayOfWeek As String = ""

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtPRFMgrps")

        dt.Columns.Add("Name", GetType(System.String))

        For Each drDSRow In ds.Tables("dtPRFMgrps").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Name") = drDSRow("Name")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function RunReport(ByVal rep As String, ByVal grp As String, ByVal curr As String, ByVal sdate As String, ByVal edate As String, ByVal per As String, ByVal reppath As String) As String
        '*********************************************************'
        '*** This sub routine creates a script to run a report ***'
        '*** for a particular account on a particular date     ***'
        '*********************************************************'
        Dim scriptrunner As String = ""

        If System.IO.File.Exists("C:\Program Files (x86)\Advent\ApxClient\4.0\AdvScriptRunner.exe") Then
            scriptrunner = "C:\Program Files (x86)\Advent\ApxClient\4.0\AdvScriptRunner "
        Else
            scriptrunner = "C:\Program Files\Advent\ApxClient\4.0\AdvScriptRunner "
        End If

        Dim progressMSG As String = ""
        Dim startfile As String = ""
        Dim filename As String = ""
        Dim ext As String = ""
        If rep = "missprice.mac" Then
            ext = ".rtf"
        ElseIf rep = "LongShortRatio.mac" Then
            ext = ".txt"
        Else
            ext = ".xls"
        End If

        Dim filecurr As String = ""
        If grp.Length > 8 Then
            If grp.Substring(6, 3) = "130" Then
                filecurr = "130" & per
            Else
                filecurr = curr
            End If
        End If

        Dim acct As String = Replace(grp, "+@", "")

        Dim tframe As String = ""
        If grp.Contains("dtd") Then
            tframe = "dtd"
        ElseIf grp.Contains("mtd") Then
            tframe = "mtd"
        ElseIf grp.Contains("qtd") Then
            tframe = "qtd"
        End If

        'creates a command line that will run an APX macro
        Dim cmd As String = ""
        cmd = scriptrunner & " REPRUN -m" & rep & " -p" & grp & " "
        Dim cmdA As String = Chr(34) & "-b" & sdate & " " & edate & Chr(34) & " -vrx -u"
        Dim cmdB As String = Chr(34) & "-b" & sdate & " " & edate & Chr(34) & " -vs -u"
        Dim cmdC As String = "-b" & sdate & " -vs -u"


        'appends output types to command line and creates output filename
        If rep.Substring(0, 6) = "PRFMc_" Then

            progressMSG = vbTab & "Investment Results (" & filecurr & ")"

            startfile = rep.Substring(0, Len(rep) - 4)
            If grp.Substring(6, 3) = "130" Then
                filename = "PRFMc_130" & per
            Else
                filename = Replace(rep.Substring(0, Len(rep) - 4), "new", "")
            End If
            cmd &= cmdB

        ElseIf rep.Substring(0, 14) = "PRFM_UpdateDay" Then

            progressMSG = "Updating Daily performance (" & curr & ") - " & sdate & " to " & edate '& vbCrLf

            startfile = rep.Substring(0, Len(rep) - 4)
            If curr = "130" Then
                filename = "PRFM_Update130"
            Else
                filename = acct & "_" & Replace(startfile, "PRFM_", "")
            End If
            cmd &= cmdB

        ElseIf rep.Substring(0, 14) = "PRFM_UpdateMon" Then

            progressMSG = "Updating Monthly performance (" & curr & ") - " & sdate & " to " & edate '& vbCrLf

            startfile = rep.Substring(0, Len(rep) - 4)
            If curr = "130" Then
                filename = "PRFM_Update130"
            Else
                filename = acct & "_" & Replace(startfile, "PRFM_", "")
            End If
            cmd &= cmdB

        ElseIf rep.Substring(0, 11) = "PRFM_Delete" Then

            progressMSG = "Deleting performance (" & curr & ") - " & sdate & " to " & edate '& vbCrLf

            startfile = rep.Substring(0, Len(rep) - 4)
            If curr = "130" Then
                filename = "PRFM_Delete130"
            Else
                filename = acct & "_" & Replace(startfile, "PRFM_", "")
            End If
            cmd &= cmdC

        ElseIf rep = "LongShortRatio.mac" Then

            progressMSG = "Long/Short ratio: "

            startfile = "LongShortRatio"
            filename = startfile
            cmd &= cmdC

        End If

        'Debug.WriteLine(cmd)
        Try
            Shell(cmd, AppWinStyle.NormalFocus, True)
            System.IO.File.Copy(reppath & startfile & ext, datafilepath & filename & ext, True)
        Catch ex As Exception
            MsgBox("CMD:" & cmd & vbCrLf & "Copy " & reppath & startfile & ext & " to " & datafilepath & filename & ext & vbCrLf & ex.ToString, MsgBoxStyle.Exclamation, "Run " & rep)
        End Try

        Return progressMSG

    End Function

    Public Function Holidays(ByVal connPort As String) As DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_GetHolidays", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        Dim DayOfWeek As String = ""

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow
        Try
            da.Fill(ds, "dtHolidays")

            dt.Columns.Add("holidaydate", GetType(System.DateTime))

            For Each drDSRow In ds.Tables("dtHolidays").Rows()
                drNewRow = dt.NewRow()
                drNewRow("holidaydate") = drDSRow("holidaydate")
                dt.Rows.Add(drNewRow)
            Next
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Information, "Holidays")
        End Try

        Return dt
    End Function

    Public Function HolidaysWeekends(ByVal connPort As String, ByVal mdate As Date)

        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_GetHolidaysWeekends", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@DATE", mdate)

        Dim DayOfWeek As String = ""

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtHolidayWeekend")

        dt.Columns.Add("Date", GetType(System.DateTime))
        dt.Columns.Add("Day", GetType(System.String))
        dt.Columns.Add("Holiday", GetType(System.String))

        For Each drDSRow In ds.Tables("dtHolidayWeekend").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Date") = drDSRow("Date")
            drNewRow("Day") = drDSRow("Day")
            drNewRow("Holiday") = drDSRow("Holiday")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetMissingIndexPrices(ByVal connPort As String, ByVal rundate As Date) As DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_CheckIndexPrices", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@DATE", rundate)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtMissDex")

        dt.Columns.Add("IndexName", GetType(System.String))

        For Each drDSRow In ds.Tables("dtMissDex").Rows()
            drNewRow = dt.NewRow()
            drNewRow("IndexName") = drDSRow("IndexName")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetCustomAccounts(ByVal connPort As String) As DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("Select * from PRFM_CustomStart", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtCustom")

        dt.Columns.Add("PerfGroup", GetType(System.String))
        dt.Columns.Add("PerfDate", GetType(System.DateTime))

        For Each drDSRow In ds.Tables("dtCustom").Rows()
            drNewRow = dt.NewRow()
            drNewRow("PerfGroup") = drDSRow("PerfGroup")
            drNewRow("PerfDate") = drDSRow("PerfDate")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetCustomStartDate(ByVal connPort As String, ByVal acct As String) As Date
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_CustomDate", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@ACCT", acct)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtPerfDate")

        dt.Columns.Add("PerfDate", GetType(System.String))

        For Each drDSRow In ds.Tables("dtPerfDate").Rows()
            drNewRow = dt.NewRow()
            drNewRow("PerfDate") = drDSRow("PerfDate")
            dt.Rows.Add(drNewRow)
        Next

        If dt.Rows.Count = 0 Then
            Return "12/31/2012"
        Else
            Return dt.Rows(0).Item("PerfDate")
        End If
    End Function

    Public Sub AddCustomStartDate(ByVal connPort As String, ByVal acct As String, ByVal stdate As Date)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_AddCustomStartDate", conn)

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@ACCT", acct)
        cmd.Parameters.AddWithValue("@DATE", stdate)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub DeleteCustomStartDate(ByVal connPort As String, ByVal acct As String)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("Delete from PRFM_PerfUpdate where PerfGroup = '" & acct & "'", conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub DeleteDisplayList(ByVal connPort As String)
        Dim sql As String = "Delete from PRFM_ListMembers"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub UpdateDisplayList(ByVal connPort As String, ByVal acct As String, ByVal inc As Date, ByVal curr As String, ByVal b1 As String, ByVal b2 As String, ByVal b3 As String, ByVal col As Integer, ByVal listorder As Integer)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_InsertDisplayList", conn)

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@Name", acct)
        cmd.Parameters.AddWithValue("@inc", inc)
        cmd.Parameters.AddWithValue("@Curr", curr)
        cmd.Parameters.AddWithValue("@b1", b1)
        cmd.Parameters.AddWithValue("@b2", b2)
        cmd.Parameters.AddWithValue("@b3", b3)
        cmd.Parameters.AddWithValue("@col", col)
        cmd.Parameters.AddWithValue("@listorder", listorder)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub DeletePerformance(ByVal connPort As String, ByVal enddate As Date)
        Dim sql As String = "Delete from PRFM_Performance where PeriodEnd = '" & enddate & "'"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Function PerfDataExists(ByVal connPort As String, ByVal perfdate As Date, ByVal acct As String, ByVal curr As String, ByVal incept As Date)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_AccountDataExists", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@perend", perfdate)
        cmd.Parameters.AddWithValue("@acct", acct)
        cmd.Parameters.AddWithValue("@curr", curr)
        cmd.Parameters.AddWithValue("@incept", incept)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim display As String = ""

        conn.Open()
        da.Fill(ds, "dtCheckPerf")
        conn.Close()

        Dim rowct As Integer = ds.Tables(0).Rows.Count

        If rowct > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub ImportPerformance(ByVal connPort As String, ByVal enddate As Date, ByVal acct As String, ByVal curr As String, ByVal per As String, ByVal perf As Decimal, ByVal incdate As Date)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_InsertPerf", conn)

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@PeriodEnd", enddate)
        cmd.Parameters.AddWithValue("@Account", acct)
        cmd.Parameters.AddWithValue("@Currency", curr)
        cmd.Parameters.AddWithValue("@Period", per)
        cmd.Parameters.AddWithValue("@Performance", perf)
        cmd.Parameters.AddWithValue("@Inception", incdate)

        'Debug.WriteLine(acct & vbTab & per & vbTab & incdate)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub UpdatePerformance(ByVal connPort As String, ByVal rundate As Date, ByVal Aytd As Decimal)
        Dim sql As String = "UPDATE PRFM_Performance " & _
                            "SET Performance = " & Aytd & " " & _
                            "WHERE Account = '130-30 Net' " & _
                            "AND PeriodEnd = '" & rundate & "' " & _
                            "AND Period = 'YTD'"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Function GetBenchmarkNames(ByVal connPort As String) As DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("select * from prfm_benchnames", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtBench")

        dt.Columns.Add("ReportName", GetType(System.String))
        dt.Columns.Add("DisplayName", GetType(System.String))

        For Each drDSRow In ds.Tables("dtBench").Rows()
            drNewRow = dt.NewRow()
            drNewRow("ReportName") = drDSRow("ReportName")
            drNewRow("DisplayName") = drDSRow("DisplayName")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetPerformance(ByVal connPort As String, ByVal rdate As Date, ByVal longshort As String) As DataSet
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_PivotPerf", conn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@date", rdate)
        cmd.Parameters.AddWithValue("@LS", longshort)

        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim display As String = ""

        If longshort = "no" Then
            display = "long only"
        ElseIf longshort = "yes" Then
            display = "short only"
        Else
            display = "all"
        End If

        Try
            conn.Open()
            da.Fill(ds, "dtPerformance")
            conn.Close()
        Catch ex As Exception
            MsgBox("Get Performance - failure" & vbCrLf & rdate & ": " & display & " " & ds.Tables.Count & " tables", MsgBoxStyle.Critical, "Unable to retrieve performance")
        End Try

        Return ds
    End Function

    Public Function GetAllPerf(ByVal connPort As String, ByVal rdate As String) As DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_AllPerf", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@date", rdate)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        Try
            da.Fill(ds, "dtAllPerf")

            dt.Columns.Add("Account", GetType(System.String))
            dt.Columns.Add("Currency", GetType(System.String))
            dt.Columns.Add("DTD", GetType(System.String))
            dt.Columns.Add("MTD", GetType(System.String))
            dt.Columns.Add("QTD", GetType(System.String))
            dt.Columns.Add("YTD", GetType(System.String))

            For Each drDSRow In ds.Tables("dtAllPerf").Rows()
                drNewRow = dt.NewRow()
                drNewRow("Account") = drDSRow("Account")
                drNewRow("Currency") = drDSRow("Currency")
                drNewRow("DTD") = drDSRow("DTD")
                drNewRow("MTD") = drDSRow("MTD")
                drNewRow("QTD") = drDSRow("QTD")
                drNewRow("YTD") = drDSRow("YTD")
                dt.Rows.Add(drNewRow)
            Next
        Catch ex As Exception
            MsgBox("Get All Perf - failure", MsgBoxStyle.Critical, "GetAllPerf")
        End Try

        Return dt
    End Function

    Public Function GetAccountList(ByVal connPort As String) As DataTable
        Dim sql As String = "select name, col, listorder " & _
                            "from dbo.PRFM_ListMembers " & _
                            "where name not like '130%' " & _
                            "UNION " & _
                            "select 'houston9' as name, 0 as col, 0 as listorder " & _
                            "from dbo.PRFM_ListMembers " & _
                            "UNION " & _
                            "select 'bomb9' as name, 0 as col, 0 as listorder " & _
                            "from dbo.PRFM_ListMembers "

        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtAcctList")

        dt.Columns.Add("Name", GetType(System.String))
        dt.Columns.Add("Col", GetType(System.String))
        dt.Columns.Add("ListOrder", GetType(System.String))

        For Each drDSRow In ds.Tables("dtAcctList").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Name") = drDSRow("Name")
            drNewRow("Col") = drDSRow("Col")
            drNewRow("ListOrder") = drDSRow("ListOrder")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetAcctGrpList(ByVal connPort As String) As DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("select displayname, name from vmAPXv4SQL.APXFirm.dbo.aoobject where name like 'prfm%'", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtPRFMgrps")

        dt.Columns.Add("DisplayName", GetType(System.String))
        dt.Columns.Add("Name", GetType(System.String))

        For Each drDSRow In ds.Tables("dtPRFMgrps").Rows()
            drNewRow = dt.NewRow()
            drNewRow("DisplayName") = drDSRow("DisplayName")
            drNewRow("Name") = drDSRow("Name")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetLastDivDate(ByVal connPort As String, ByVal rundate As Date) As Date
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_ImportDividendDate", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@date", rundate)

        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtDivDate")

        dt.Columns.Add("LastDivDate", GetType(System.String))

        For Each drDSRow In ds.Tables("dtDivDate").Rows()
            drNewRow = dt.NewRow()
            drNewRow("LastDivDate") = drDSRow("LastDivDate")
            dt.Rows.Add(drNewRow)
        Next

        Return dt.Rows(0).Item("LastDivDate")
    End Function


End Class
