Public Class StartDates
    Dim getdata As DataAccess = New DataAccess
    Dim edly As Date
    Dim connPM As String

    Public Property ConnectionPM() As String
        Get
            Return connPM
        End Get
        Set(ByVal Value As String)
            connPM = Value
        End Set
    End Property

    Private Sub StartDates_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        edly = Convert.ToDateTime("12/31/" & DatePart(DateInterval.Year, Today()) - 1)
        Me.lblInfo.Text = "Accounts will be updated from " & edly & ", unless specified otherwise (to remove a custom date, enter " & edly & ")."
        Dim dtUpdGrps As DataTable = New DataTable
        dtUpdGrps = getdata.GetPRFMGroups(connPM)
        Dim grp As String = ""

        Dim x As Integer

        For x = 0 To dtUpdGrps.Rows.Count - 1
            grp = dtUpdGrps.Rows(x).Item("Name")

            If grp.Contains("PRFMa") Or grp.Contains("PRFMb") Or grp.Contains("PRFMi") Or grp.Contains("prfmD") Or grp.Contains("prfmM") Or grp.Contains("prfmQ") Or grp.Contains("prfm") Then
                Me.cbxAccounts.Items.Add(grp)
            End If
        Next

        ShowCustomAccounts()
    End Sub

    Private Sub ShowCustomAccounts()
        Dim dtCustom As DataTable = New DataTable
        dtCustom = getdata.GetCustomAccounts(connPM)
        Me.dgCustomStart.DataSource = dtCustom
    End Sub

    Private Sub cbxAccounts_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxAccounts.SelectionChangeCommitted
        LoadStartDates()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If Me.cbxAccounts.Text = "Select update group" Then
            MsgBox("You must select a group", MsgBoxStyle.Critical, "Invalid selection")
        Else
            Dim acct As String = Me.cbxAccounts.Text
            Dim stdate As Date = Me.txtStartDate.Text

            Dim data As DataAccess = New DataAccess

            If stdate = edly Then
                data.DeleteCustomStartDate(connPM, acct)
            Else
                data.AddCustomStartDate(connPM, acct, stdate)
            End If
        End If
        ShowCustomAccounts()
    End Sub

    Private Sub LoadStartDates()
        Dim grp As String = Me.cbxAccounts.SelectedItem.ToString
        Dim startdate As Date = Convert.ToDateTime(getdata.GetCustomStartDate(connPM, grp.ToUpper))
        Me.txtStartDate.Text = startdate
    End Sub

End Class