<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StartDates
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.cbxAccounts = New System.Windows.Forms.ComboBox()
        Me.txtStartDate = New System.Windows.Forms.TextBox()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.dgCustomStart = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.dgCustomStart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblInfo
        '
        Me.lblInfo.Location = New System.Drawing.Point(13, 13)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(330, 29)
        Me.lblInfo.TabIndex = 0
        '
        'cbxAccounts
        '
        Me.cbxAccounts.FormattingEnabled = True
        Me.cbxAccounts.Location = New System.Drawing.Point(16, 45)
        Me.cbxAccounts.Name = "cbxAccounts"
        Me.cbxAccounts.Size = New System.Drawing.Size(207, 21)
        Me.cbxAccounts.TabIndex = 1
        Me.cbxAccounts.Text = "Select update group"
        '
        'txtStartDate
        '
        Me.txtStartDate.Location = New System.Drawing.Point(230, 45)
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.Size = New System.Drawing.Size(113, 20)
        Me.txtStartDate.TabIndex = 2
        '
        'btnUpdate
        '
        Me.btnUpdate.BackColor = System.Drawing.Color.SteelBlue
        Me.btnUpdate.Location = New System.Drawing.Point(268, 71)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 3
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = False
        '
        'dgCustomStart
        '
        Me.dgCustomStart.AllowUserToAddRows = False
        Me.dgCustomStart.AllowUserToDeleteRows = False
        Me.dgCustomStart.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.dgCustomStart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgCustomStart.Location = New System.Drawing.Point(17, 100)
        Me.dgCustomStart.Name = "dgCustomStart"
        Me.dgCustomStart.ReadOnly = True
        Me.dgCustomStart.Size = New System.Drawing.Size(327, 160)
        Me.dgCustomStart.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.AutoSize = True
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.Location = New System.Drawing.Point(17, 153)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(0, 0)
        Me.Panel1.TabIndex = 5
        '
        'StartDates
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.SteelBlue
        Me.ClientSize = New System.Drawing.Size(356, 278)
        Me.Controls.Add(Me.dgCustomStart)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.txtStartDate)
        Me.Controls.Add(Me.cbxAccounts)
        Me.Controls.Add(Me.lblInfo)
        Me.Name = "StartDates"
        Me.Text = "Custom Start Dates"
        CType(Me.dgCustomStart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents cbxAccounts As System.Windows.Forms.ComboBox
    Friend WithEvents txtStartDate As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents dgCustomStart As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
