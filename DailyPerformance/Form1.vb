Imports System.Math
'Imports System.Globalization
'Imports System.Exception
Imports Microsoft.Office.Interop
'Imports Microsoft.Office.Interop.Excel
'Imports Microsoft.Office.Interop.Outlook

Public Class Form1

#Region "Variables"

    Dim getdata As DataAccess = New DataAccess

    Dim todaydate As Date = Today()
    Dim rundate As Date = DateAdd(DateInterval.Day, -1, todaydate)
    Dim logdate As String = DateAdd(DateInterval.Day, -1, todaydate).ToString("MMddyy")
    Dim rdate As Date
    Dim rundateSTR As String = ""
    Dim dayB4rundate As Date
    Dim dayB4rundateSTR As String = ""
    Dim updDate As Date
    Dim saveday As String = ""
    Dim MTDFrom As Date
    Dim QTDFrom As Date
    Dim YTDFrom As Date
    Dim Q1date As Date
    Dim Q2date As Date
    Dim Q3date As Date
    Dim Q4date As Date

    Dim xlsfile As String = ""
    Dim user As String = ""
    Dim username As String = ""
    Dim userpath As String = ""
    Dim repfolder As String = ""
    Dim filepath As String = "\\fsw02\Advent\APXReports\"
    Dim defpath As String = "\\apxapp15\apx$\Default\"
    Dim reppath As String = "\\fsw02\Advent\APXReports\"
    Dim scriptrunner As String = "C:\Program Files (x86)\Advent\ApxClient\"

    Dim objDir As System.IO.DirectoryInfo
    Dim objFileI As System.IO.FileInfo
    Dim objFiles() As System.IO.FileInfo
    Dim objLOG As System.IO.TextWriter

    Dim weblink As String = "http://commonshare/collaborations/DailyPerformance/"
    'Dim webpath As String = "M:\Web Collaborations\DailyPerformance\"
    Dim webpath As String = "c:\temp\DailyPerf\WebPath\"
    'Dim datafilepath As String = "\\fsw02\Department\Web Collaborations\DailyPerformance\zUpdateFiles\DataFiles\"
    Dim datafilepath As String = "c:\temp\dailyperf\DataFiles\"
    'Dim refpath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\ReferenceFiles\"
    Dim refpath As String = "c:\temp\DailyPerf\ReferenceFiles\"
    'Dim dailyperfpath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\OttoUpdateFiles\"
    Dim dailyperfpath As String = "c:\temp\dailyperf\OttoUpdateFiles\"
    'Dim archivepath As String = "M:\Temporary Data Holding\DailyPerformance\"
    Dim archivepath As String = "c:\temp\DailyPerformance\"
    'Dim batpath As String = "M:\Temporary Data Holding\Jesica\VBApplications\BatchFiles\"
    Dim batpath As String = "c:\temp\dailyperf\BatchFiles\"

    Dim batfile As String = "DailyPerformance.bat"
    Dim displayfile As String = "DailyPerformanceLayout.xlsx"
    Dim abbrfile As String = "NameAbbreviations.xlsx"
    Dim batdelete As String = "DeleteNetPerformance.bat"
    Dim objBAT As System.IO.TextWriter

    Dim SecondIndexCheck As Boolean = False

    Dim emaillist As String

#Region "Arrays & Hashtables"
    Dim dtRerunGroups As New Data.DataTable("RerunGroups")
    Dim ht As Hashtable
    Dim htGroups As Hashtable = New Hashtable
    Dim htMissingIndexPrices As Hashtable = New Hashtable
    Dim htMissingSecurityPrices As Hashtable = New Hashtable
    Dim htMissDex2 As Hashtable = New Hashtable
    Dim dtHolidays As Data.DataTable = New Data.DataTable
    Dim htHolidays As Hashtable = New Hashtable
    Dim htCustom As Hashtable = New Hashtable
    Dim dtAcctList As Data.DataTable = New Data.DataTable
    Dim dtBenchList As Data.DataTable = New Data.DataTable
    Dim htAcctList As Hashtable = New Hashtable
    Dim htBenchList As Hashtable = New Hashtable
    Dim htBenchPerf As Hashtable = New Hashtable
    Dim htColStart As Hashtable = New Hashtable
    Dim htIndex As Hashtable = New Hashtable
    Dim htMainName_ACCT As Hashtable = New Hashtable
    Dim htBBNames_ACCT As Hashtable = New Hashtable
    Dim htMainName_BNCH As Hashtable = New Hashtable
    Dim htBBNames_BNCH As Hashtable = New Hashtable
    Dim cur() As String = {"USD", "CAD", "GBP", "EUR", "JPY"}
    Dim per() As String = {"DTD", "MTD", "QTD", "YTD"}
    Dim aryUpdDate() As Date
#End Region

#Region "Connection Strings"
    Dim connPM As String = ""
    Dim connDM As String = ""
    Dim connQA As String = ""
    Dim connAPX As String = ""
    Public Sub GetConnectionStrings()
        'Dim conpath As String = "\\coder\Information Systems\Jesica\"
        Dim conpath As String = "\\SSIS16\Development\Configurations\"
        Dim objTR As System.IO.TextReader
        objTR = System.IO.File.OpenText(conpath & "SQLConnections.ini")
        While objTR.Peek <> -1
            Dim splitout As String() = Split(objTR.ReadLine(), vbTab)
            If splitout(0) = "PortMGMT" Then
                connPM = splitout(1)
            End If
            If splitout(0) = "DataMGMT" Then
                connDM = splitout(1)
            End If
            If splitout(0) = "QACustom" Then
                connQA = splitout(1)
            End If
            If splitout(0) = "APX" Then
                connAPX = splitout(1)
            End If
        End While
        objTR.Close()
    End Sub
#End Region

#End Region

#Region "Form Load Events"

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        GetConnectionStrings()
        Debug.WriteLine("")
        Debug.WriteLine("*****************************************************")
        objLOG = System.IO.File.AppendText(dailyperfpath & "UpdatePerf " & rundateSTR & ".log")

        Me.dtpRunDate.Value = rundate

        Dim i As Integer
        'load holidays into hashtable, to determine run dates
        dtHolidays = getdata.Holidays(connPM)
        For i = 0 To dtHolidays.Rows.Count - 1
            htHolidays.Add(dtHolidays.Rows(i).Item("HolidayDate"), dtHolidays.Rows(i).Item("HolidayDate"))
        Next

        GetRunDates()
        GetCustomDateAccounts()
        LoadReportFolders()
        DeleteNetPerformance()

        Dim htUsers As Hashtable = New Hashtable
        'htUsers.Add("jcarleton", "Jesica")
        htUsers.Add("dburdick", "Dave")

        user = System.Environment.UserName

        'If running from Otto account or Database use the APX default for the report folder path, else if the username is in the 
        'admin Hashtable use the defined report folder path. All else expects a defined report path.
        If System.Environment.UserName = "otto" Or System.Environment.UserName = "database" Then
            repfolder = defpath
        ElseIf htUsers.ContainsKey(System.Environment.UserName) = True Then
            repfolder = reppath & htUsers(user) & "\"
            user = htUsers(user)
        Else
            repfolder = reppath & System.Environment.UserName & "\"
        End If
        lblFolderPath.Text = repfolder

        Dim XLSAppNames As New Excel.Application
        Dim WkBkNames As Workbook = XLSAppNames.Workbooks.Open(refpath & abbrfile)
        Dim WkShtNames As Worksheet = CType(WkBkNames.Worksheets(1), Worksheet)

        Dim maxrow As Integer = WkShtNames.UsedRange.Count
        Dim readrow As Integer = 2
        Dim reportname, mainname, bbname As String
        For readrow = 2 To maxrow
            If WkShtNames.Cells(readrow, 1).value <> "" Then
                reportname = WkShtNames.Cells(readrow, 2).value
                mainname = WkShtNames.Cells(readrow, 3).value
                bbname = WkShtNames.Cells(readrow, 4).value

                If WkShtNames.Cells(readrow, 1).value = "A" Then

                    If htMainName_ACCT.ContainsKey(reportname) = False Then
                        htMainName_ACCT.Add(reportname, mainname)
                    End If

                    If htBBNames_ACCT.ContainsKey(reportname) = False Then
                        htBBNames_ACCT.Add(reportname, bbname)
                    End If

                Else
                    If htMainName_BNCH.ContainsKey(reportname) = False Then
                        htMainName_BNCH.Add(reportname, mainname)
                    End If

                    If htBBNames_BNCH.ContainsKey(reportname) = False Then
                        htBBNames_BNCH.Add(reportname, bbname)
                    End If
                End If

            End If
        Next

        WkBkNames.Close()
        WkBkNames = Nothing
        XLSAppNames = Nothing

        Me.txtDivs.Text = getdata.GetLastDivDate(connPM, rundate)

        LoadAccountList()
        GetCustomDateAccounts()

        'Dim LogInfo As String = ""
        'LogInfo = CheckLogFile()
        'Me.txtProgress.Text &= vbCrLf & LogInfo
        'ScrollToBottom()

        'Me.txtProgress.Text &= vbCrLf
    End Sub

    Private Sub LoadAccountList()
        Dim dsList As DataSet = New DataSet

        ' Call GetAccountsIndices to fill two tables. One for accounts and one for benchmarks. It also designates a listing order?
        dsList = getdata.GetAccountsIndices(connPM)
        dtAcctList = dsList.Tables(0)
        dtBenchList = dsList.Tables(1)

        'Fill 
        For i As Integer = 0 To dtAcctList.Rows.Count - 1
            If htAcctList.ContainsKey(dtAcctList.Rows(i).Item("Name").toupper) = False Then
                htAcctList.Add(dtAcctList.Rows(i).Item("Name").toupper, dtAcctList.Rows(i).Item("Name"))
                Debug.WriteLine(dtAcctList.Rows(i).Item("Name").toupper)

                If dtAcctList.Rows(i).Item("ListOrder") = 1 Then
                    If htColStart.ContainsKey(dtAcctList.Rows(i).Item("Name").ToUpper) = False Then
                        htColStart.Add(dtAcctList.Rows(i).Item("Name").toupper, dtAcctList.Rows(i).Item("col"))
                    End If
                End If
            End If
        Next

        For b As Integer = 0 To dtBenchList.Rows.Count - 1
            If htBenchList.ContainsKey(dtBenchList.Rows(b).Item("Name").toupper) = False Then
                htBenchList.Add(dtBenchList.Rows(b).Item("Name").toupper, dtBenchList.Rows(b).Item("Name"))
                Debug.WriteLine(dtBenchList.Rows(b).Item("Name").toupper)
            End If
        Next
    End Sub

    Private Function CheckLogFile() As String
        Dim loginfo As String = ""
        Dim findSTR As Integer = 0
        Dim finddash As Integer = 0
        Dim findgrp As String = ""
        Dim UpdCt As Integer = 0
        Dim FailCt As Integer = 0
        Dim dateCt As Integer = 0

        Dim missingindex As Boolean = False
        Dim missingprice As Boolean = False
        Dim ShowDMEmail As Boolean = False
        Dim missingpriceCT As Integer = 0
        Dim ftseprice As Double = 0.0

        Dim objTR As System.IO.TextReader

        If System.IO.File.Exists(dailyperfpath & "UpdatePerf " & logdate & ".log") Then

            ''loginfo = "** MORNING LOG FILE **" & vbCrLf
            objTR = System.IO.File.OpenText(dailyperfpath & "UpdatePerf " & logdate & ".log")

            Dim strLine As String = ""
            dtRerunGroups.Columns.Add("Name")

            While objTR.Peek <> -1
                strLine = objTR.ReadLine()

                If htIndex.ContainsKey(strLine.ToUpper) = True Then
                    missingindex = True
                    If strLine.ToUpper = "FTSEBLEND" Then
                        'ftseprice = getdata.GetFTSEprice()
                        htMissingIndexPrices.Add(strLine.ToUpper, ftseprice)
                    Else
                        htMissingIndexPrices.Add(strLine.ToUpper, strLine.ToUpper)
                    End If
                    loginfo &= strLine & vbCrLf
                End If

                If strLine = "There are missing prices that must be updated" Then
                    ShowDMEmail = True
                    missingprice = True
                    loginfo &= strLine & vbCrLf
                ElseIf strLine = "Performance could not be updated:" Then
                    ShowDMEmail = True
                    missingprice = False
                    loginfo &= strLine & vbCrLf
                ElseIf strLine.Contains("UPDATE:") = False Then
                    loginfo &= strLine & vbCrLf
                End If


                If Len(strLine) >= 4 Then

                    If strLine.Substring(0, 4) = "prfm" Then


                        findSTR = strLine.IndexOf(":")
                        findgrp = strLine.Substring(0, findSTR).ToUpper

                        updDate = strLine.Substring(findSTR + 2, Len(strLine) - findSTR - 2)

                        If strLine.Contains("UPDATE: ") = True And strLine.Contains("*** ") = True And htCustom.ContainsKey(findgrp) = False Then
                            FailCt += 1
                        ElseIf strLine.Contains("UPDATE: ") = True And strLine.Contains("*** ") = True And htCustom.ContainsKey(findgrp) = True Then
                            UpdCt += 1
                        ElseIf strLine.Contains("UPDATE: ") = True And strLine.Contains("*** ") = False Then
                            UpdCt += 1
                        End If

                        ReDim Preserve aryUpdDate(dateCt)
                        aryUpdDate(dateCt) = updDate
                        dateCt += 1
                        'UpdCt -= 1

                        If htGroups.ContainsKey(findgrp) = False Then
                            dtRerunGroups.Rows.Add(New Object() {findgrp})
                        End If

                    End If
                End If


                If Len(strLine) >= 7 Then
                    If strLine.Substring(0, 7) = "ERROR: " Then

                        findSTR = strLine.IndexOf(" file not exported.")
                        findgrp = strLine.Substring(7, Len(strLine) - findSTR - 5).ToUpper
                        'UpdCt -= 1

                        If htGroups.ContainsKey(findgrp) = True Then
                            dtRerunGroups.Rows.Add(New Object() {findgrp})
                        End If

                    End If
                End If

                If missingprice = True Then
                    If strLine <> "There are missing prices that must be updated" And strLine <> "" Then
                        htMissingSecurityPrices.Add(strLine, strLine)
                    End If
                End If
            End While


            If ShowDMEmail = True Then
                Me.btnEmailDM.Visible = True
            End If


            objTR.Close()

            If UpdCt > 0 Then
                loginfo &= UpdCt & " accounts/groups have been updated."
            End If

            If FailCt > 0 Then
                loginfo &= vbCrLf & FailCt & " accounts/groups failed to update."
            End If

        Else
            loginfo = "No log file, please confirm that nightly update completed."
        End If

        If loginfo = "" Then
            loginfo = "Log file is empty."
        End If

        Return loginfo
    End Function

    Private Sub GetCustomDateAccounts()
        Dim getdata As DataAccess = New DataAccess
        Dim dtCustom As Data.DataTable = New Data.DataTable
        dtCustom = getdata.GetCustomAccounts(connPM)

        Dim x As Integer = 0
        For x = 0 To dtCustom.Rows.Count - 1
            If htCustom.ContainsKey(dtCustom.Rows(x).Item("PerfGroup")) = False Then
                htCustom.Add(dtCustom.Rows(x).Item("PerfGroup"), dtCustom.Rows(x).Item("PerfGroup"))
            End If
        Next
    End Sub

    Private Sub LoadReportFolders()
        Dim fs As Object
        Dim fo As Object
        Dim x As Object
        Dim folder As String
        ht = New Hashtable

        fs = CreateObject("Scripting.FileSystemObject")
        fo = fs.GetFolder(filepath)

        For Each x In fo.SubFolders
            folder = x.Name
            ht.Add(folder.ToUpper, folder)
        Next
    End Sub

    Public Sub ScrollToBottom()
        txtProgress.SelectionStart = 0
        txtProgress.SelectionStart = txtProgress.TextLength
        txtProgress.ScrollToCaret()
    End Sub

    Private Sub GetReportFolder()
        Dim user As String = Me.txtRepFolder.Text
        If ht.ContainsKey(user.ToUpper) = False Then
            MsgBox("Not a valid report output folder", MsgBoxStyle.Critical, "Invalid Entry")
            Me.txtRepFolder.Clear()
            Me.txtRepFolder.Focus()
        Else
            user = ht.Item(user.ToUpper)
            If user = "otto" Or user = "database" Then
                reppath = defpath & "\"
            Else
                reppath = filepath & user & "\"
            End If
            Me.lblFolderPath.Text = reppath
        End If
    End Sub

    Private Sub DeleteNetPerformance()
        Me.txtProgress.Text &= vbCrLf & "Deleting net performance start dates"
        RefreshApp()

        'Dim curr As String = ""
        'Dim macro As String = ""
        'Dim cmd As String = ""

        'objBAT = System.IO.File.CreateText(batpath & batdelete)
        'objBAT.WriteLine("C:")
        'objBAT.WriteLine("cd " & Chr(34) & scriptrunner & Chr(34))
        'objBAT.WriteLine("")

        'For c As Integer = 0 To cur.GetUpperBound(0)
        '    curr = cur(c)
        '    macro = "PRFM_Delete" & curr & "_Net"
        '    cmd = "AdvScriptRunner REPRUN -m" & macro & " -p@PRFM_" & curr & " " & "-b123110 -vs -u"
        '    objBAT.WriteLine(cmd)


        'Next
        'objBAT.Close()

        Try

            Dim myProcess As Process = New Process
            myProcess.StartInfo.FileName = batpath & batdelete
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            myProcess.StartInfo.CreateNoWindow = True
            myProcess.Start()
            myProcess.WaitForExit()
            myProcess.Close()
            System.Threading.Thread.Sleep(20000)

            Me.txtProgress.Text &= vbCrLf & "Net performance deleted"
            RefreshApp()

        Catch ex As SystemException

            objLOG.WriteLine("Error running batch file" & vbCrLf & ex.ToString, "Delete Net Performance")

        End Try

    End Sub

#End Region

#Region "Form Events"

    Private Sub btnRunRep_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnRunRep.Click
        Dim getdata As DataAccess = New DataAccess
        getdata.DeletePerformance(connPM, rundate)
        Me.txtProgress.Text &= vbCrLf & "Creating batch file:"
        objBAT = System.IO.File.CreateText(batpath & batfile)
        objBAT.WriteLine("C:")
        objBAT.WriteLine("cd " & Chr(34) & scriptrunner & Chr(34))
        objBAT.WriteLine("")
        LongShortPerformanceReports()
        PerformanceReports()
        Me.txtProgress.Text &= vbCrLf & vbTab & vbTab & "Batch file complete."
        ScrollToBottom()
        RefreshApp()
        objBAT.Close()


        Try

            Me.txtProgress.Text &= vbCrLf & "Running batch file:"
            Dim myProcess As Process = New Process
            myProcess.StartInfo.FileName = batpath & batfile
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            myProcess.StartInfo.CreateNoWindow = True
            myProcess.Start()
            myProcess.WaitForExit()
            myProcess.Close()
            System.Threading.Thread.Sleep(20000)
            Me.txtProgress.Text &= vbCrLf & vbTab & vbTab & "Reports complete."

        Catch ex As SystemException

            objLOG.WriteLine("Error running batch file" & vbCrLf & ex.ToString, "ERROR running reports")

        End Try

        YTD130Perf()
        ProcessPerformanceReports()
        CreateOutputFile()
    End Sub

    Private Sub ProcessPerformanceReports()

        Dim XLSAppLOAD As New Excel.Application
        Dim filecurr As String = ""
        Dim maxrow As Integer
        Dim readrow As Integer
        Dim readcell1 As String = ""
        Dim readcell2 As String = ""
        Dim fullname As String = ""
        Dim shortname As String = ""
        Dim benchname As String = ""
        Dim acctlist As String = ""
        Dim acctperfrow As Integer
        Dim labelrow As Integer
        Dim dtdcol, mtdcol, qtdcol, ytdcol, itdcol As Integer
        Dim dtd, mtd, qtd, ytd As Boolean
        Dim a_d, a_m, a_q, a_y, a_i As Decimal
        Dim b_d, b_m, b_q, b_y, b_i As Decimal
        Dim incept As String = ""
        Dim htMissPrice As Hashtable = New Hashtable

        For c As Integer = 0 To cur.GetUpperBound(0)
            filecurr = cur(c)
            Debug.WriteLine("")
            Debug.WriteLine(filecurr & "***************************************************************************")

            Dim WkBkLOAD As Workbook = XLSAppLOAD.Workbooks.Open(datafilepath & "PRFMc_" & filecurr & ".xlsx")
            Dim WkShtLOAD As Worksheet = CType(WkBkLOAD.Worksheets(1), Worksheet)

            maxrow = WkShtLOAD.UsedRange.Count

            For readrow = 1 To maxrow
                readcell1 = WkShtLOAD.Cells(readrow, 1).value

                If readcell1 <> "" Then
                    fullname = readcell1
                    shortname = Replace(Replace(Replace(Replace(readcell1, " (DTD)", ""), " (MTD)", ""), " (QTD)", ""), " (YTD)", "")
                    If shortname = "Univest9" Then
                        MsgBox(fullname)
                    End If
                    If htAcctList.Contains(shortname.ToUpper()) Then 'GET ACCOUNT DATA
                        acctperfrow = readrow + 5
                        If shortname = "NASDAQ COMP" Then
                            acctperfrow += 1
                        End If
                        labelrow = readrow + 2
                        dtd = False
                        mtd = False
                        qtd = False
                        ytd = False

                        For col As Integer = 2 To WkShtLOAD.UsedRange.Columns.Count
                            If WkShtLOAD.Cells(labelrow, col).value = "Date" Then
                                dtdcol = col
                                dtd = True
                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Month" Then
                                mtdcol = col
                                mtd = True
                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Quarter" Then
                                qtdcol = col
                                qtd = True
                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Year" Then
                                ytdcol = col
                                ytd = True
                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Inception" Then
                                itdcol = col
                                incept = WkShtLOAD.Cells(labelrow + 1, itdcol).value
                            End If
                        Next

                        If fullname.Contains("DTD") Then
                            If dtd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, dtdcol).value) <> "?" Then
                                    a_d = WkShtLOAD.Cells(acctperfrow, dtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "DTD", a_d, incept)
                                End If
                            End If
                        ElseIf fullname.Contains("MTD") Then
                            If mtd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, mtdcol).value) <> "?" Then
                                    a_m = WkShtLOAD.Cells(acctperfrow, mtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "MTD", a_m, incept)
                                End If
                            End If
                        ElseIf fullname.Contains("QTD") Then
                            If qtd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, qtdcol).value) <> "?" Then
                                    a_q = WkShtLOAD.Cells(acctperfrow, qtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "QTD", a_q, incept)
                                End If
                            End If
                        ElseIf fullname.Contains("YTD") Then
                            If ytd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, ytdcol).value) <> "?" Then
                                    a_y = WkShtLOAD.Cells(acctperfrow, ytdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "YTD", a_y, incept)
                                End If
                            End If

                            If Convert.ToString(WkShtLOAD.Cells(acctperfrow, itdcol).value) <> "?" Then
                                a_i = WkShtLOAD.Cells(acctperfrow, itdcol).value
                                getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "ITD", a_i, incept)
                            End If
                        Else
                            If dtd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, dtdcol).value) <> "?" Then
                                    a_d = WkShtLOAD.Cells(acctperfrow, dtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "DTD", a_d, incept)
                                End If
                            End If
                            If mtd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, mtdcol).value) <> "?" Then
                                    a_m = WkShtLOAD.Cells(acctperfrow, mtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "MTD", a_m, incept)
                                End If
                            End If
                            If qtd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, qtdcol).value) <> "?" Then
                                    a_q = WkShtLOAD.Cells(acctperfrow, qtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "QTD", a_q, incept)
                                End If
                            End If
                            If ytd = True Then
                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, ytdcol).value) <> "?" Then
                                    a_y = WkShtLOAD.Cells(acctperfrow, ytdcol).value
                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "YTD", a_y, incept)
                                End If
                            End If
                            If Convert.ToString(WkShtLOAD.Cells(acctperfrow, itdcol).value) <> "?" Then
                                a_i = WkShtLOAD.Cells(acctperfrow, itdcol).value
                                getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "ITD", a_i, incept)
                            End If
                        End If


                    ElseIf htBenchList.Contains(readcell1.ToUpper()) Then 'GET INDEX DATA

                        benchname = readcell1

                        If Convert.ToString(WkShtLOAD.Cells(readrow, dtdcol).value) = "?" Or Convert.ToString(WkShtLOAD.Cells(readrow, mtdcol).value) = "?" Or Convert.ToString(WkShtLOAD.Cells(readrow, qtdcol).value) = "?" Or Convert.ToString(WkShtLOAD.Cells(readrow, ytdcol).value) = "?" Then
                            If htMissPrice.ContainsKey(benchname) = False Then
                                Me.txtProgress.Text &= vbCrLf & benchname
                                htMissPrice.Add(benchname, benchname)
                            End If

                        Else

                            If Not getdata.PerfDataExists(connPM, rundate, benchname, filecurr, incept) Then
                                If dtd = True Then
                                    b_d = WkShtLOAD.Cells(readrow, dtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "DTD", b_d, incept)
                                Else
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "DTD", Nothing, incept)
                                End If

                                If mtd = True Then
                                    b_m = WkShtLOAD.Cells(readrow, mtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "MTD", b_m, incept)
                                Else
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "MTD", Nothing, incept)
                                End If

                                If qtd = True Then
                                    b_q = WkShtLOAD.Cells(readrow, qtdcol).value
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "QTD", b_q, incept)
                                Else
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "QTD", Nothing, incept)
                                End If

                                If ytd = True Then
                                    b_y = WkShtLOAD.Cells(readrow, ytdcol).value
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "YTD", b_y, incept)
                                Else
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "YTD", Nothing, incept)
                                End If

                                If Convert.ToString(WkShtLOAD.Cells(readrow, itdcol).value) <> "?" Then
                                    b_i = WkShtLOAD.Cells(readrow, itdcol).value
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "ITD", b_i, incept)
                                Else
                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "ITD", Nothing, incept)
                                End If

                            End If
                        End If

                    End If
                End If

            Next
            WkBkLOAD.Close()
        Next

        XLSAppLOAD = Nothing
        Me.txtProgress.Text &= vbTab & vbCrLf & "Import complete."
        ScrollToBottom()
        RefreshApp()
    End Sub

    Private Sub PerformanceReports()
        ScrollToBottom()
        RefreshApp()
        Dim q As Integer = 0
        Dim curr As String = ""
        Dim rep As String = ""
        For q = 0 To cur.GetUpperBound(0)
            curr = cur(q)
            rep = "PRFMc_" & curr & ".mac"

            Me.txtProgress.Text &= vbCrLf & CreateBatFile(rep, "@prfm_" & curr, curr, dayB4rundateSTR, rundateSTR, "", repfolder)
            ScrollToBottom()
            RefreshApp()
        Next
    End Sub

    Private Sub LongShortPerformanceReports()

        Dim MTDstr As String = MTDFrom.ToString("MMddyy")
        Dim QTDstr As String = QTDFrom.ToString("MMddyy")
        Dim YTDstr As String = YTDFrom.ToString("MMddyy")

        Dim p As Integer
        Dim fromdateSTR As String = ""
        For p = 0 To per.GetUpperBound(0)
            Select Case per(p)
                Case "DTD"
                    fromdateSTR = dayB4rundateSTR
                Case "MTD"
                    fromdateSTR = MTDstr
                Case "QTD"
                    fromdateSTR = QTDstr
                Case "YTD"
                    fromdateSTR = YTDstr
            End Select

            Me.txtProgress.Text &= vbCrLf & CreateBatFile("PRFMc_130.mac", "@prfm_130", "USD", fromdateSTR, rundateSTR, per(p), repfolder)

        Next
        'Me.txtProgress.Text &= vbCrLf & CreateBatFile("PRFMc_13030.mac", "@prfm_130", "USD", YTDstr, rundateSTR, "30ytd", repfolder)

    End Sub

    Private Sub YTD130Perf()
        Me.txtProgress.Text &= vbCrLf & "Compiling 130-30 performance"
        Dim incept As Date = Convert.ToDateTime("9/30/2016")
        Dim YTDstr As String = YTDFrom.ToString("MMddyy")
        Dim p As Integer
        Dim XLSApp As New Excel.Application

        For p = 0 To per.GetUpperBound(0)

            Dim WkBkLOAD As Workbook = XLSApp.Workbooks.Open(datafilepath & "PRFMc_130" & per(p) & ".xlsx")
            Dim WkShtLOAD As Worksheet = CType(WkBkLOAD.Worksheets(1), Worksheet)


            Dim rngNet As Excel.Range = GetSpecifiedRange("TOTAL PORTFOLIO", WkShtLOAD)
            Dim NetRow As Integer = rngNet.Row
            Dim rngLong As Excel.Range = GetSpecifiedRange("LONG Total", WkShtLOAD)
            Dim LongRow As Integer = rngLong.Row
            Dim rngShort As Excel.Range = GetSpecifiedRange("SHORT Total", WkShtLOAD)
            Dim ShortRow As Integer = rngShort.Row

            getdata.ImportPerformance(connPM, rundate, "130-30 Net", "USD", per(p), WkShtLOAD.Cells(NetRow, 8).value, incept)
            getdata.ImportPerformance(connPM, rundate, "130-30 Long", "USD", per(p), WkShtLOAD.Cells(LongRow, 8).value, incept)
            getdata.ImportPerformance(connPM, rundate, "130-30 Short", "USD", per(p), WkShtLOAD.Cells(ShortRow, 8).value, incept)

            ScrollToBottom()
            WkBkLOAD.Close()
        Next

        'Dim WkBkYear As Microsoft.Office.Interop.Excel.Workbook = XLSApp.Workbooks.Open(datafilepath & "PRFMc_13030ytd.xlsx")
        'Dim WkShtYear As Microsoft.Office.Interop.Excel.Worksheet = CType(WkBkYear.Worksheets(1), Microsoft.Office.Interop.Excel.Worksheet)
        'ScrollToBottom()

        ''to calculate YTD figures

        'Dim Aq1 As Decimal = 0.0
        'Dim Aq2 As Decimal = 0.0
        'Dim Aq3 As Decimal = 0.0
        'Dim Aqtd As Decimal = 0.0
        'Dim Aytd As Decimal = 0.0
        'Dim missingdata As String = ""
        'If Convert.IsDBNull(WkShtYear.Cells(11, 2).value) Then
        '    Aqtd = 0
        'Else
        '    Aqtd = WkShtYear.Cells(11, 2).value
        'End If
        'If Convert.IsDBNull(WkShtYear.Cells(11, 3).value) Then
        '    Aq1 = 0
        'Else
        '    Aq1 = WkShtYear.Cells(11, 3).value
        'End If
        'If Convert.IsDBNull(WkShtYear.Cells(11, 4).value) Then
        '    Aq2 = 0
        'Else
        '    Aq2 = WkShtYear.Cells(11, 4).value
        'End If
        'If Convert.IsDBNull(WkShtYear.Cells(11, 5).value) Then
        '    Aq3 = 0
        'Else
        '    Aq3 = WkShtYear.Cells(11, 5).value
        'End If
        'If rundate = Q1date Then
        '    Aq1 = 0
        'ElseIf rundate = Q2date Then
        '    Aq2 = 0
        'ElseIf rundate = Q3date Then
        '    Aq3 = 0
        'End If

        'If missingdata <> "" Then
        '    MsgBox("There are missing data, therefore YTD performance will not be accurate: " & vbCrLf & missingdata, MsgBoxStyle.Information, "Missing Data")
        'End If
        'Aytd = Math.Round(((1 + Aq1 / 100) * (1 + Aq2 / 100) * (1 + Aq3 / 100) * (1 + Aqtd / 100) - 1) * 100, 2)


        'getdata.UpdatePerformance(connPM, rundate, Aytd)

        'WkBkYear.Close()
        'WkShtYear = Nothing
        'WkBkYear = Nothing
        XLSApp.Quit()
        XLSApp = Nothing
    End Sub

    Public Function CreateBatFile(ByVal rep As String, ByVal grp As String, ByVal curr As String, ByVal sdate As String, ByVal edate As String, ByVal per As String, ByVal reppath As String) As String
        '*********************************************************'
        '*** This sub routine creates a script to run a report ***'
        '*** for a particular account on a particular date     ***'
        '*********************************************************'

        Dim progressMSG As String = ""
        Dim startfile As String = ""
        Dim filename As String = ""
        Dim ext As String = ""
        If rep = "missprice.mac" Then
            ext = ".rtf"
        ElseIf rep = "LongShortRatio.mac" Then
            ext = ".txt"
        Else
            ext = ".xlsx"
        End If

        Dim filecurr As String = ""
        If grp.Length > 8 Then
            If grp.Substring(6, 3) = "130" Then
                filecurr = "130" & per
            Else
                filecurr = curr
            End If
        End If

        Dim acct As String = Replace(grp, "+@", "")

        Dim tframe As String = ""
        If grp.Contains("dtd") Then
            tframe = "dtd"
        ElseIf grp.Contains("mtd") Then
            tframe = "mtd"
        ElseIf grp.Contains("qtd") Then
            tframe = "qtd"
        End If

        'creates a command line that will run an APX macro
        Dim cmd As String = "AdvScriptRunner REPRUN -m" & rep & " -p" & grp & " "
        Dim cmdA As String = Chr(34) & "-b" & sdate & " " & edate & Chr(34) & " -vrx -u"
        Dim cmdB As String = Chr(34) & "-b" & sdate & " " & edate & Chr(34) & " -vs -u"
        Dim cmdC As String = "-b" & sdate & " -vs -u"


        'appends output types to command line and creates output filename
        If rep.Substring(0, 6) = "PRFMc_" Then

            progressMSG = vbTab & "Investment Results (" & filecurr & ")"

            startfile = rep.Substring(0, Len(rep) - 4)
            If grp.Substring(6, 3) = "130" Then
                filename = "PRFMc_130" & per
            Else
                filename = Replace(rep.Substring(0, Len(rep) - 4), "new", "")
            End If
            cmd &= cmdB

        ElseIf rep.Substring(0, 14) = "PRFM_UpdateDay" Then

            progressMSG = "Updating Daily performance (" & curr & ") - " & sdate & " to " & edate '& vbCrLf

            startfile = rep.Substring(0, Len(rep) - 4)
            If curr = "130" Then
                filename = "PRFM_Update130"
            Else
                filename = acct & "_" & Replace(startfile, "PRFM_", "")
            End If
            cmd &= cmdB

        ElseIf rep.Substring(0, 14) = "PRFM_UpdateMon" Then

            progressMSG = "Updating Monthly performance (" & curr & ") - " & sdate & " to " & edate '& vbCrLf

            startfile = rep.Substring(0, Len(rep) - 4)
            If curr = "130" Then
                filename = "PRFM_Update130"
            Else
                filename = acct & "_" & Replace(startfile, "PRFM_", "")
            End If
            cmd &= cmdB

        ElseIf rep.Substring(0, 11) = "PRFM_Delete" Then

            progressMSG = "Deleting performance (" & curr & ") - " & sdate & " to " & edate '& vbCrLf

            startfile = rep.Substring(0, Len(rep) - 4)
            If curr = "130" Then
                filename = "PRFM_Delete130"
            Else
                filename = acct & "_" & Replace(startfile, "PRFM_", "")
            End If
            cmd &= cmdC

        ElseIf rep = "LongShortRatio.mac" Then

            progressMSG = "Long/Short ratio: "

            startfile = "LongShortRatio"
            filename = startfile
            cmd &= cmdC

        End If


        objBAT.WriteLine(cmd)
        objBAT.WriteLine("copy /y " & repfolder & startfile & ext & " " & Chr(34) & datafilepath & filename & ext & Chr(34))
        objBAT.WriteLine("")

        Return progressMSG

    End Function

    Private Sub btnFolder_Click(ByVal sender As System.Object, ByVal e As EventArgs)
        fbdFolder = New FolderBrowserDialog
        fbdFolder.ShowDialog()
        fbdFolder.SelectedPath = reppath
        reppath = fbdFolder.SelectedPath
    End Sub

    Private Sub txtRepFolder_TextChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles txtRepFolder.TextChanged
        'GetReportFolder()
        user = Me.txtRepFolder.Text
        If user = "otto" Or user = "database" Then
            reppath = defpath
            Me.lblFolderPath.Text = reppath
        Else
            Me.lblFolderPath.Text = reppath & Me.txtRepFolder.Text
        End If

        'userpath = filepath & Me.txtRepFolder.Text & "\"
        'Me.lblFolderPath.Text = userpath

        RefreshApp()

    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnFind.Click
        GetReportFolder()
    End Sub

    Private Sub lnkLog_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkLog.LinkClicked
        Process.Start(dailyperfpath & "UpdatePerf " & logdate & ".log")
    End Sub

    Private Sub CreateOutputFile()
        Me.txtProgress.Text &= vbCrLf & "Creating output file."
        ScrollToBottom()
        RefreshApp()

        Dim XLSApp As New Excel.Application
        Dim booksWRITE As Workbooks = XLSApp.Workbooks
        Dim WkBkWRITE As _Workbook = booksWRITE.Add()
        Dim WkShtWRITE As Worksheet = CType(WkBkWRITE.Worksheets(1), Worksheet)

        WkShtWRITE.Cells.Interior.Color = RGB(255, 255, 255)

        WkShtWRITE.Range("A1").Value = "AM Performance Numbers * Gross of fees"
        WkShtWRITE.Range("A1").Cells.Font.Size = 12
        WkShtWRITE.Range("A1").Font.Bold = True
        WkShtWRITE.Range("A1").Font.Italic = True
        'WkShtWRITE.Range("E1").Value = "NEW"
        WkShtWRITE.Range("G1").Value = "Institutional accounts versus their benchmark"
        WkShtWRITE.Range("G1").Font.Bold = True
        WkShtWRITE.Range("A2").Value = rundate.ToString("D")
        WkShtWRITE.Range("A2").Font.Bold = True
        WkShtWRITE.Range("A3").Value = "Trading Group"
        WkShtWRITE.Range("B3").Value = "DTD"
        WkShtWRITE.Range("C3").Value = "MTD"
        WkShtWRITE.Range("D3").Value = "QTD"
        WkShtWRITE.Range("E3").Value = "YTD"
        WkShtWRITE.Range("B3:E3").Font.Bold = True
        WkShtWRITE.Range("G3").Value = "Trading Group"
        WkShtWRITE.Range("H3").Value = "DTD"
        WkShtWRITE.Range("I3").Value = "MTD"
        WkShtWRITE.Range("J3").Value = "QTD"
        WkShtWRITE.Range("K3").Value = "YTD"
        WkShtWRITE.Range("H3:K3").Font.Bold = True


        Dim startcol As String = ""
        Dim endcol As String = ""
        Dim column As Integer = 1
        Dim curr As String = "" 'Currency
        Dim acct As String = "" 'Account
        Dim a_d As Decimal = 0.0 'date to date
        Dim a_m As Decimal = 0.0 'month to date
        Dim a_q As Decimal = 0.0 'quarter to date
        Dim a_y As Decimal = 0.0 'year to date
        Dim a_i As Decimal = 0.0
        Dim dtd As Boolean = False
        Dim mtd As Boolean = False
        Dim qtd As Boolean = False
        Dim ytd As Boolean = False
        Dim b As Integer = 0
        Dim B1 As String = ""
        Dim b1_d As Decimal = 0.0 'date to date
        Dim b1_m As Decimal = 0.0 'month to date
        Dim b1_q As Decimal = 0.0 'quarter to date
        Dim b1_y As Decimal = 0.0 'year to date
        Dim b1_i As Decimal = 0.0
        Dim B2 As String = ""
        Dim b2_d As Decimal = 0.0 'date to date
        Dim b2_m As Decimal = 0.0 'month to date
        Dim b2_q As Decimal = 0.0 'quarter to date
        Dim b2_y As Decimal = 0.0
        Dim b2_i As Decimal = 0.0
        Dim B3 As String = ""
        Dim b3_d As Decimal = 0.0 'date to date
        Dim b3_m As Decimal = 0.0 'month to date
        Dim b3_q As Decimal = 0.0 'quarter to date
        Dim b3_y As Decimal = 0.0 'year to date
        Dim b3_i As Decimal = 0.0
        Dim outrow As Integer = 4
        Dim startrow As Integer = 1
        Dim endrow As Integer = 1
        Dim numbench As Integer = 0
        Dim endcol1 As Integer = 1
        Dim endcol2 As Integer = 1
        Dim getdata As DataAccess = New DataAccess
        Dim ds As DataSet = New DataSet
        ds = getdata.GetPerformance(connPM, rundate, "no")
        Dim missingdata As String = ""

        Dim dc As Integer = 0
        For dc = 0 To ds.Tables.Count - 1
            If ds.Tables(dc).Rows.Count > 0 Then
                acct = ds.Tables(dc).Rows(0).Item("Account")
                curr = RTrim(LTrim(ds.Tables(dc).Rows(0).Item("Currency")))

                dtd = False
                mtd = False
                qtd = False
                ytd = False

                If IsDBNull(ds.Tables(dc).Rows(0).Item("ITD")) Then
                    a_i = 0
                Else
                    a_i = ds.Tables(dc).Rows(0).Item("ITD")
                End If

                If IsDBNull(ds.Tables(dc).Rows(0).Item("DTD")) Then
                    a_d = 0
                    dtd = False
                Else
                    a_d = ds.Tables(dc).Rows(0).Item("DTD")
                    dtd = True
                End If

                If IsDBNull(ds.Tables(dc).Rows(0).Item("MTD")) Then
                    a_m = a_i
                    mtd = False
                Else
                    a_m = ds.Tables(dc).Rows(0).Item("MTD")
                    mtd = True
                End If

                If IsDBNull(ds.Tables(dc).Rows(0).Item("QTD")) Then
                    a_q = a_i
                    qtd = False
                Else
                    a_q = ds.Tables(dc).Rows(0).Item("QTD")
                    qtd = True
                End If

                If IsDBNull(ds.Tables(dc).Rows(0).Item("YTD")) Then
                    a_y = a_i
                    ytd = False
                Else
                    a_y = ds.Tables(dc).Rows(0).Item("YTD")
                    ytd = True
                End If

                If htColStart.ContainsKey(acct.ToUpper) Then
                    If htColStart(acct.ToUpper) = "2" Then
                        column = 7
                        endcol1 = outrow - 1
                    End If
                    outrow = 4
                End If

                If column = 1 Then
                    startcol = "A"
                    endcol = "E"
                Else
                    startcol = "G"
                    endcol = "K"
                End If

                startrow = outrow

                WkShtWRITE.Cells(outrow, column).value = acct
                WkShtWRITE.Cells(outrow, column).Font.Bold = True
                WkShtWRITE.Cells(outrow, column + 1).value = a_d
                WkShtWRITE.Cells(outrow, column + 2).value = a_m
                WkShtWRITE.Cells(outrow, column + 3).value = a_q
                WkShtWRITE.Cells(outrow, column + 4).value = a_y

                Dim rng As Excel.Range
                numbench = ds.Tables(dc).Rows.Count - 1
                If numbench = 0 Then
                    WkShtWRITE.Range(startcol & outrow.ToString & ":" & endcol & outrow.ToString).Interior.Color = RGB(217, 217, 217)
                End If

                outrow += 1
                If numbench >= 1 Then
                    B1 = RTrim(LTrim(ds.Tables(dc).Rows(1).Item("Account")))
                    If IsDBNull(ds.Tables(dc).Rows(1).Item("ITD")) Then
                        b1_i = 0.0
                    Else
                        b1_i = ds.Tables(dc).Rows(1).Item("ITD")
                    End If

                    If htMainName_BNCH.ContainsKey(B1) Then
                        B1 = htMainName_BNCH(B1)
                    End If
                    b1_d = ds.Tables(dc).Rows(1).Item("DTD")

                    If mtd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(1).Item("MTD")) Then
                            b1_m = b1_d
                        Else
                            b1_m = ds.Tables(dc).Rows(1).Item("MTD")
                        End If
                    Else
                        b1_m = b1_i
                    End If

                    If qtd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(1).Item("QTD")) Then
                            b1_q = b1_m
                        Else
                            b1_q = ds.Tables(dc).Rows(1).Item("QTD")
                        End If
                    Else
                        b1_q = b1_i
                    End If

                    If ytd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(1).Item("YTD")) Then
                            b1_y = b1_q
                        Else
                            b1_y = ds.Tables(dc).Rows(1).Item("YTD")
                        End If
                    Else
                        b1_y = b1_i
                    End If

                    WkShtWRITE.Cells(outrow, column).value = B1
                    WkShtWRITE.Cells(outrow, column).Font.Bold = True
                    rng = XLSApp.Range(startcol & outrow.ToString)
                    rng.HorizontalAlignment = Excel.Constants.xlRight
                    WkShtWRITE.Range(startcol & outrow.ToString & ":" & endcol & outrow.ToString).Interior.Color = RGB(217, 217, 217)
                    WkShtWRITE.Cells(outrow, column + 1).value = b1_d
                    WkShtWRITE.Cells(outrow, column + 2).value = b1_m
                    WkShtWRITE.Cells(outrow, column + 3).value = b1_q
                    WkShtWRITE.Cells(outrow, column + 4).value = b1_y
                    outrow += 1

                    WkShtWRITE.Cells(outrow, column).value = "diff"
                    WkShtWRITE.Cells(outrow, column).Font.Bold = True
                    rng = XLSApp.Range(startcol & outrow.ToString)
                    rng.HorizontalAlignment = Excel.Constants.xlRight

                    WkShtWRITE.Cells(outrow, column + 1).value = a_d - b1_d
                    WkShtWRITE.Cells(outrow, column + 2).value = a_m - b1_m
                    WkShtWRITE.Cells(outrow, column + 3).value = a_q - b1_q
                    WkShtWRITE.Cells(outrow, column + 4).value = a_y - b1_y
                    outrow += 1
                End If

                If numbench >= 2 Then
                    B2 = RTrim(LTrim(ds.Tables(dc).Rows(2).Item("Account")))
                    If IsDBNull(ds.Tables(dc).Rows(2).Item("ITD")) Then
                        b2_i = 0.0
                    Else
                        b2_i = ds.Tables(dc).Rows(2).Item("ITD")
                    End If

                    If htMainName_BNCH.ContainsKey(B2) Then
                        B1 = htMainName_BNCH(B2)
                    End If
                    b2_d = ds.Tables(dc).Rows(2).Item("DTD")

                    If mtd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(2).Item("MTD")) Then
                            b2_m = b2_d
                        Else
                            b2_m = ds.Tables(dc).Rows(2).Item("MTD")
                        End If
                    Else
                        b2_m = b2_i
                    End If

                    If qtd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(2).Item("QTD")) Then
                            b2_q = b2_i
                        Else
                            b2_q = ds.Tables(dc).Rows(2).Item("QTD")
                        End If
                    Else
                        b2_q = b2_i
                    End If

                    If ytd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(2).Item("YTD")) Then
                            b2_y = b2_i
                        Else
                            b2_y = ds.Tables(dc).Rows(2).Item("YTD")
                        End If
                    Else
                        b2_y = b2_i
                    End If

                    WkShtWRITE.Cells(outrow, column).value = B2
                    WkShtWRITE.Cells(outrow, column).Font.Bold = True
                    rng = XLSApp.Range(startcol & outrow.ToString)
                    rng.HorizontalAlignment = Excel.Constants.xlRight
                    WkShtWRITE.Range(startcol & outrow.ToString & ":" & endcol & outrow.ToString).Interior.Color = RGB(217, 217, 217)
                    WkShtWRITE.Cells(outrow, column + 1).value = b2_d
                    WkShtWRITE.Cells(outrow, column + 2).value = b2_m
                    WkShtWRITE.Cells(outrow, column + 3).value = b2_q
                    WkShtWRITE.Cells(outrow, column + 4).value = b2_y
                    outrow += 1

                    WkShtWRITE.Cells(outrow, column).value = "diff"
                    WkShtWRITE.Cells(outrow, column).Font.Bold = True
                    rng = XLSApp.Range(startcol & outrow.ToString)
                    rng.HorizontalAlignment = Excel.Constants.xlRight

                    WkShtWRITE.Cells(outrow, column + 1).value = a_d - b2_d
                    WkShtWRITE.Cells(outrow, column + 2).value = a_m - b2_m
                    WkShtWRITE.Cells(outrow, column + 3).value = a_q - b2_q
                    WkShtWRITE.Cells(outrow, column + 4).value = a_y - b2_y

                    outrow += 1
                End If

                If numbench >= 3 Then
                    B3 = RTrim(LTrim(ds.Tables(dc).Rows(3).Item("Account")))
                    If IsDBNull(ds.Tables(dc).Rows(3).Item("ITD")) Then
                        b3_i = 0.0
                    Else
                        b3_i = ds.Tables(dc).Rows(3).Item("ITD")
                    End If

                    If htMainName_BNCH.ContainsKey(B3) Then
                        B3 = htMainName_BNCH(B3)
                    End If
                    b3_d = ds.Tables(dc).Rows(3).Item("DTD")

                    If mtd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(3).Item("MTD")) Then
                            b3_m = b3_i
                        Else
                            b3_m = ds.Tables(dc).Rows(3).Item("MTD")
                        End If
                    Else
                        b3_m = b3_i
                    End If

                    If qtd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(3).Item("QTD")) Then
                            b3_q = b3_m
                        Else
                            b3_q = ds.Tables(dc).Rows(3).Item("QTD")
                        End If
                    Else
                        b3_q = b3_i
                    End If

                    If ytd = True Then
                        If IsDBNull(ds.Tables(dc).Rows(3).Item("YTD")) Then
                            b3_y = b3_i
                        Else
                            b3_y = ds.Tables(dc).Rows(3).Item("YTD")
                        End If
                    Else
                        b3_y = b3_i
                    End If

                    WkShtWRITE.Cells(outrow, column).value = B3
                    WkShtWRITE.Cells(outrow, column).Font.Bold = True
                    rng = XLSApp.Range(startcol & outrow.ToString)
                    rng.HorizontalAlignment = Excel.Constants.xlRight
                    WkShtWRITE.Range(startcol & outrow.ToString & ":" & endcol & outrow.ToString).Interior.Color = RGB(217, 217, 217)
                    WkShtWRITE.Cells(outrow, column + 1).value = b3_d
                    WkShtWRITE.Cells(outrow, column + 2).value = b3_m
                    WkShtWRITE.Cells(outrow, column + 3).value = b3_q
                    WkShtWRITE.Cells(outrow, column + 4).value = b3_y
                    outrow += 1

                    WkShtWRITE.Cells(outrow, column).value = "diff"
                    WkShtWRITE.Cells(outrow, column).Font.Bold = True
                    rng = XLSApp.Range(startcol & outrow.ToString)
                    rng.HorizontalAlignment = Excel.Constants.xlRight

                    WkShtWRITE.Cells(outrow, column + 1).value = a_d - b3_d
                    WkShtWRITE.Cells(outrow, column + 2).value = a_m - b3_m
                    WkShtWRITE.Cells(outrow, column + 3).value = a_q - b3_q
                    WkShtWRITE.Cells(outrow, column + 4).value = a_y - b3_y
                    outrow += 1
                End If

                endrow = startrow + (numbench * 2)
                outrow = endrow + 2

                'Debug.WriteLine(acct & vbTab & startrow & vbTab & endrow)
                If dc = ds.Tables.Count - 1 Then
                    endcol2 = outrow - 1
                End If


                With WkShtWRITE.Range(startcol & startrow, endcol & endrow)
                    .Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
                    .Borders(XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
                    .Borders(XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
                    .Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
                End With
            End If
        Next

        WkShtWRITE.Cells.Font.Name = "Arial"
        WkShtWRITE.Cells.Font.Size = 10
        WkShtWRITE.Cells.NumberFormat = "#0.00"
        WkShtWRITE.Range("A1").Cells.Font.Size = 12

        With WkShtWRITE.Range("B4:E" & endcol1.ToString)
            .FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, 0)
            .FormatConditions(1).Font.Color = RGB(0, 204, 102)

            .FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLessEqual, 0)
            .FormatConditions(2).Font.Color = RGB(255, 0, 0)
        End With

        With WkShtWRITE.Range("H4:K" & endcol2.ToString)
            .FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, 0)
            .FormatConditions(1).Font.Color = RGB(0, 204, 102)

            .FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLessEqual, 0)
            .FormatConditions(2).Font.Color = RGB(255, 0, 0)
        End With

        WkShtWRITE.Columns.ColumnWidth = 5.5
        WkShtWRITE.Range("A:A").ColumnWidth = 30
        WkShtWRITE.Range("G:G").ColumnWidth = 30

        Dim lastrow As Integer = Max(endcol1, endcol2)
        outrow = lastrow + 1
        WkShtWRITE.Cells(outrow, 7).value = "NOTE: Values are in USD unless otherwise indicated."
        outrow += 1
        WkShtWRITE.Cells(outrow, 7).value = "Dividends last posted as of " & Me.txtDivs.Text & "."
        WkShtWRITE.Range("G" & outrow.ToString & ":K" & outrow.ToString).Cells.Merge()
        WkShtWRITE.Cells(outrow, 7).Font.Bold = True
        outrow += 1
        WkShtWRITE.Cells(outrow, 1).value = "For internal use only.  May not be redistributed without Compliance approval."
        WkShtWRITE.Cells(outrow, 1).Font.Bold = True

        With WkShtWRITE.Range("A" & outrow.ToString & ":K" & outrow.ToString)
            .Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        End With

        With WkShtWRITE.Range("A1:K" & outrow.ToString)
            .Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        End With


        Me.txtProgress.Text &= vbCrLf & vbTab & "Output file complete."
        ScrollToBottom()
        RefreshApp()
        XLSApp.DisplayAlerts = False
        WkBkWRITE.SaveAs(webpath & "DailyPerformance.xlsx")
        WkBkWRITE.SaveAs(webpath & saveday & "\" & saveday & "Confirmed.htm", FileFormat:=XlFileFormat.xlHtml)
        WkBkWRITE.Close()
        XLSApp.DisplayAlerts = True

        XLSApp = Nothing
        KillExcel()

        Process.Start(webpath & "DailyPerformance.xlsx")
    End Sub

    Private Function GetSpecifiedRange(ByVal matchStr As String, ByVal objWs As Worksheet) As Excel.Range
        Dim currentFind As Excel.Range = Nothing
        Dim firstFind As Excel.Range = Nothing
        currentFind = objWs.Range("A1:AM1000").Find(matchStr, , XlFindLookIn.xlValues, XlLookAt.xlPart, XlSearchOrder.xlByRows, XlSearchDirection.xlNext, False)
        Return currentFind
    End Function

    Sub KillExcel()
        Dim oExcelApp As Object
        oExcelApp = GetObject(, "Excel.Application")
        oExcelApp.Quit()
        oExcelApp = Nothing
    End Sub

#End Region

#Region "Update Performance"

    Public Sub CheckUpdateStatus(ByVal curr As String)

        Dim XLSApp As New Excel.Application
        Dim WkBk As Workbook
        Dim WkSht As Worksheet


        Dim misslist As String = ""
        Dim missct As Integer = 0
        Dim failurelist As String = ""
        Dim failurect As Integer = 0
        Dim htMiss As Hashtable = New Hashtable

        Dim typesymb As String = ""
        Dim sectype As String = ""
        Dim symbol As String = ""
        Dim mdate As String = ""
        Dim acct As String = ""
        Dim reason As String = ""
        Dim row As Integer = 1
        Dim filename As String = ""

        objDir = New IO.DirectoryInfo(repfolder)
        objFiles = objDir.GetFiles("PRFM_Update" & curr & ".xlsx")

        For Each objFileI In objFiles
            filename = objFileI.FullName
            objLOG.WriteLine(filename)

            WkBk = XLSApp.Workbooks.Open(filename)
            WkSht = WkBk.ActiveSheet

            For row = 1 To WkSht.UsedRange.Rows.Count + 2
                If WkSht.Cells(row, 1).value = "Warning: Missing price for " Then

                    typesymb = WkSht.Cells(row, 3).value
                    mdate = WkSht.Cells(row, 5).value

                    If htMiss.ContainsKey(typesymb & mdate) = False Then
                        htMiss.Add(typesymb & mdate, typesymb & mdate)
                        missct += 1
                        sectype = typesymb.Substring(0, 4)
                        symbol = typesymb.Substring(4, Len(typesymb) - 4)
                        reason = "Missing price"
                        misslist &= sectype & vbTab & symbol & vbTab & mdate & vbCrLf
                    End If

                ElseIf InStr(WkSht.Cells(row, 1).value, "Cannot update") Then

                    failurect += 1

                    If WkSht.Cells(row, 4).value = "because of a date gap." Then
                        mdate = WkSht.Cells(row + 1, 4).value
                        reason = "Date gap"
                        ''ElseIf WkSht.Cells(row, 4).value = "" And WkSht.Cells(row, 5).value = "because of a date gap." Then
                        ''    mdate = WkSht.Cells(row + 1, 4).value
                        ''    reason = "Date gap"
                    ElseIf WkSht.Cells(row, 4).value = "to " Then
                        mdate = WkSht.Cells(row, 5).value
                        reason = WkSht.Cells(row, 6).value
                    Else 'If WkSht.Cells(row, 5).value = "to" Then
                        mdate = WkSht.Cells(row, 6).value
                        reason = WkSht.Cells(row, 7).value
                    End If

                    acct = Replace(WkSht.Cells(row, 2).value, " performance (gross)", "")
                    failurelist &= acct & ": " & mdate & " - " & reason & vbCrLf
                    row += 1

                End If
            Next

            WkSht = Nothing
            WkBk.Save()
            WkBk.Close()

        Next


        If missct > 0 Then
            Me.txtProgress.Text &= vbCrLf & "There are missing prices that must be updated!"
            Me.txtProgress.Text &= vbCrLf & misslist
            ScrollToBottom()
        End If

        If failurect > 0 Then
            Me.txtProgress.Text &= vbCrLf & "Performance could not be updated:"
            Me.txtProgress.Text &= vbCrLf & failurelist
            ScrollToBottom()
        End If

        If missct = 0 And failurect = 0 Then
            Me.txtProgress.Text &= vbCrLf & curr & " performance updated through " & rundate & "."
            ScrollToBottom()
        End If

        'objLOG.Close()
        WkBk = Nothing
        XLSApp.Quit()
        XLSApp = Nothing

    End Sub

    Private Sub GetPreviousDaysPrices(ByVal htMiss As Hashtable, ByVal mdate As Date)
        Dim getdata As DataAccess = New DataAccess
        Dim dtHolWeek As Data.DataTable = New Data.DataTable
        dtHolWeek = getdata.HolidaysWeekends(connPM, mdate)

        Dim mday As String
        Dim expday As Date
        Dim i As Integer
        For i = 0 To dtHolWeek.Rows.Count - 1
            mday = dtHolWeek.Rows(i).Item("Day")
            If mday = "Monday" Then
                expday = DateAdd(DateInterval.Day, -3, mdate)
            ElseIf mday = "Sunday" Then
                expday = DateAdd(DateInterval.Day, -2, mdate)
            Else
                expday = DateAdd(DateInterval.Day, -1, mdate)
            End If
        Next
    End Sub

    Public Sub RefreshApp()
        System.Windows.Forms.Application.DoEvents()
        Me.Refresh()
    End Sub

#End Region

#Region "Dates"

    Private Sub GetRunDates()
        'rundate = DateAdd(DateInterval.Day, -1, todaydate)
        rundate = Me.dtpRunDate.Value
        If htHolidays.ContainsKey(rundate) Then
            rundate = DateAdd(DateInterval.Day, -2, todaydate)
        End If

        If rundate.DayOfWeek = 0 Then
            rundate = DateAdd(DateInterval.Day, -2, rundate)
        End If

        dayB4rundate = DateAdd(DateInterval.Day, -1, rundate)

        'Debug.WriteLine("Run date: " & rundate)
        'Debug.WriteLine("Day b4 run date: " & dayB4rundate)


        If dayB4rundate.DayOfWeek = 0 Then
            dayB4rundate = DateAdd(DateInterval.Day, -2, dayB4rundate)
        End If
        saveday = rundate.DayOfWeek.ToString

        rundateSTR = rundate.ToString("MMddyy")
        dayB4rundateSTR = dayB4rundate.ToString("MMddyy")

        lblRunDate.Text = "For periods ending: " '& rundate.ToString.Substring(0, Len(rundate.ToString) - 11)
        'Me.dtpRunDate.Value = rundate

        Dim day As Integer = DatePart(DateInterval.Day, rundate)
        Dim mon As Integer = DatePart(DateInterval.Month, rundate)
        Dim year As Integer = DatePart(DateInterval.Year, rundate)

        Select Case mon
            Case 1
                MTDFrom = Convert.ToDateTime("12/31/" & year - 1)
                QTDFrom = Convert.ToDateTime("12/31/" & year - 1)
            Case 2
                MTDFrom = Convert.ToDateTime("1/31/" & year)
                QTDFrom = Convert.ToDateTime("12/31/" & year - 1)
            Case 3
                Select Case year
                    Case "2008", "2012", "2016", "2020"
                        MTDFrom = Convert.ToDateTime("2/29/" & year)
                    Case Else
                        MTDFrom = Convert.ToDateTime("2/28/" & year)
                End Select
                QTDFrom = Convert.ToDateTime("12/31/" & year - 1)
            Case 4
                MTDFrom = Convert.ToDateTime("3/31/" & year)
                QTDFrom = Convert.ToDateTime("3/31/" & year)
            Case 5
                MTDFrom = Convert.ToDateTime("4/30/" & year)
                QTDFrom = Convert.ToDateTime("3/31/" & year)
            Case 6
                MTDFrom = Convert.ToDateTime("5/31/" & year)
                QTDFrom = Convert.ToDateTime("3/31/" & year)
            Case 7
                MTDFrom = Convert.ToDateTime("6/30/" & year)
                QTDFrom = Convert.ToDateTime("6/30/" & year)
            Case 8
                MTDFrom = Convert.ToDateTime("7/31/" & year)
                QTDFrom = Convert.ToDateTime("6/30/" & year)
            Case 9
                MTDFrom = Convert.ToDateTime("8/31/" & year)
                QTDFrom = Convert.ToDateTime("6/30/" & year)
            Case 10
                MTDFrom = Convert.ToDateTime("9/30/" & year)
                QTDFrom = Convert.ToDateTime("9/30/" & year)
            Case 11
                MTDFrom = Convert.ToDateTime("10/31/" & year)
                QTDFrom = Convert.ToDateTime("9/30/" & year)
            Case 12
                MTDFrom = Convert.ToDateTime("11/30/" & year)
                QTDFrom = Convert.ToDateTime("9/30/" & year)
        End Select

        YTDFrom = Convert.ToDateTime("12/31/" & year - 1)
        Q1date = Convert.ToDateTime("3/31/" & year)
        Q2date = Convert.ToDateTime("6/30/" & year)
        Q3date = Convert.ToDateTime("9/30/" & year)
        Q4date = Convert.ToDateTime("12/31/" & year)

        'If rundate = Q1date Or rundate = Q2date Or rundate = Q3date Or rundate = Q4date Then
        '    Debug.WriteLine(rundate)
        'End If

        'Debug.WriteLine("Run date: " & rundate)
        'Debug.WriteLine("Day b4 run date: " & dayB4rundate)

    End Sub

    Private Function GetDate(ByVal last As Date, ByVal per As Char) As Date
        Dim monthend As Date
        Dim qtrend As Date
        Dim yearend As Date
        Dim mon As String = ""
        Dim year As String = ""
        Dim day As String = ""
        Dim m As Integer = DatePart(DateInterval.Month, last)
        Dim y As Integer = DatePart(DateInterval.Year, last)

        If per = "M" Then
            Select Case m
                Case 1
                    mon = "12"
                    day = "31"
                    year = y - 1
                Case Else

                    mon = Convert.ToString(m - 1)
                    Select Case m
                        Case 2, 4, 6, 8, 9, 11
                            day = "31"
                        Case 5, 7, 10, 12
                            day = "30"
                        Case 3
                            Select Case y
                                Case 2000, 2004, 2008, 2012, 2016, 2020
                                    day = "28"
                                Case Else
                                    day = "29"
                            End Select
                    End Select
                    year = y.ToString
            End Select

            monthend = Convert.ToDateTime(mon & "/" & day & "/" & y)
            Return monthend
        ElseIf per = "Q" Then
            Select Case m
                Case 1, 2, 3
                    mon = "12"
                    day = "31"
                    year = y - 1
                Case 4, 5, 6
                    mon = "3"
                    day = "31"
                    year = y
                Case 7, 8, 9
                    mon = "6"
                    day = "30"
                    year = y
                Case 10, 11, 12
                    mon = "9"
                    day = "30"
                    year = y
            End Select

            qtrend = Convert.ToDateTime(mon & "/" & day & "/" & y)
            Return qtrend
        Else
            yearend = Convert.ToDateTime("12/31/" & y - 1)
            Return yearend
        End If
    End Function

    Private Sub dtpRunDate_ValueChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles dtpRunDate.ValueChanged
        rundate = Me.dtpRunDate.Value
        dayB4rundate = DateAdd(DateInterval.Day, -1, rundate)
        rundateSTR = rundate.ToString("MMddyy")
        dayB4rundateSTR = dayB4rundate.ToString("MMddyy")
    End Sub

#End Region

#Region "Send Emails"

    Private Sub btnEmailDM_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnEmailDM.Click
        SendDMEmail(htMissingIndexPrices, htMissingSecurityPrices)
    End Sub

    Private Sub SendDMEmail(ByVal indexlist As Hashtable, ByVal pricelist As Hashtable)
        Dim msg As String = ""
        Dim o As Integer = 0
        Dim Enumerator As IDictionaryEnumerator

        msg = "<html>" &
              "<head><title>Daily Performance - missing data</title></head>" &
              "<body>" &
              "<table>"

        Dim htIndexList As Hashtable = New Hashtable
        If SecondIndexCheck = False Then
            htIndexList = htMissingIndexPrices
        Else
            htIndexList = htMissDex2
        End If
        If htIndexList.Count > 0 Then
            msg &= "<tr><b><td>Please update index prices for " & rundate & ": </td></b></tr>"

            Enumerator = htIndexList.GetEnumerator()

            While Enumerator.MoveNext()
                msg &= "<tr><td>" & Enumerator.Value.ToString() & "</td></tr>"
            End While
        End If


        msg &= "<tr><td height=25></td></tr>"

        If htMissingSecurityPrices.Count > 0 Then
            msg &= "<tr><b><td>Please update security prices: </td></b></tr>"

            Enumerator = htMissingSecurityPrices.GetEnumerator()

            While Enumerator.MoveNext()
                msg &= "<tr><td>" & Enumerator.Value.ToString() & "</td></tr>"
            End While
        End If

        msg &= "<tr><td height=25></td></tr>"
        msg &= "<tr><td>Thank you,</td></tr>"
        msg &= "<tr><td>" & user & "</td></tr>"
        msg &= "</table>" &
                "</body>" &
                "</html>"


        Dim Outl As Object
        Outl = CreateObject("Outlook.Application")
        If Outl IsNot Nothing Then
            Dim omsg As Object
            omsg = Outl.CreateItem(0)
            omsg.To = "DataMgmt@mckinleycapital.com"
            omsg.Subject = "Daily Performance - missing data"
            omsg.BodyFormat = OlBodyFormat.olFormatHTML
            omsg.HTMLBody = msg
            omsg.Display(True)
        End If
    End Sub

    Private Sub btnEmail_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnEmail.Click
        Dim CustomMsg As String = Me.txtCustomMsg.Text
        emaillist = ""
        If Me.cbxTest.Checked = True Then
            emaillist = "TEST"
        End If
        If Me.cbxDaily.Checked = True Then
            Email_Main(CustomMsg, emaillist)
        End If
        If Me.cbxBob.Checked = True Then
            Email_Bob(CustomMsg, emaillist)
        End If
        If Me.cbxBB.Checked = True Then
            Email_Blackberry(CustomMsg, emaillist)
        End If
        If Me.cbx130.Checked = True Then
            Email_130(CustomMsg, emaillist)
        End If
    End Sub

    Private Sub Email_Main(ByVal msg As String, ByVal emaillist As String)
        Dim htmlpage1 As String = "<html>" &
                            "<head>" &
                            "<title>AM Performance</title>" &
                            "</head>" &
                            "<body>" &
                            "	<table rules=none border=0 frame=box>" &
                            "   <FONT SIZE=3>" &
                            "		<tr>" &
                            "				<Label id=verify>" & msg & "</Label>" &
                            "		</td></tr>" &
                            "		<tr>" &
                            "				<Label id=verify>" & weblink & saveday & "/" & saveday & "confirmed.htm</Label>" &
                            "		</td></tr>" &
                            "	</FONT>" &
                            "   </table>" &
                            "</body>" &
                            "</html>"

        Dim xlsfile As String = webpath & "DailyPerformance.xlsx"
        If emaillist = "" Then emaillist = "MAIN"
        SendEmail(rundate & " AM Performance", emaillist, xlsfile, htmlpage1)
    End Sub

    Private Sub Email_Bob(ByVal msg As String, ByVal emaillist As String)
        Dim readBob As String = ""

        readBob = msg & vbCrLf & "<html>" &
                            "<head>" &
                            "<title>YTD Performance for Bob</title>" &
                            "</head>" &
                            "<body>" &
                            "	<table rules=none border=1 frame=box><FONT SIZE=3>" &
                            "		<tr><td colspan=2><B>AM Perf Numbers * Gross</B></td></tr>" &
                            "		<tr><td colspan=2><B>" & rundate & "</B></td></tr>"

        readBob &= "		<tr bgcolor=#87CEFA><B>" &
                          "<td WIDTH=150 ALIGN=left style=white-space: wrap>ACCOUNT</td>" &
                          "<td WIDTH=55 ALIGN=center style=white-space: nowrap>YTD</td>" &
                          "</B></tr>" &
                 "		<tr bgcolor=#000000><td colspan=2></td></tr>"


        Dim acct As String = ""
        Dim a_y As Decimal = 0.0
        Dim B As String = ""
        Dim b_y As Decimal = 0.0

        Dim getdata As DataAccess = New DataAccess
        Dim ds As DataSet = New DataSet
        ds = getdata.GetPerformance(connPM, rundate, "no")
        Dim tc As Integer = 0
        Dim numbench As Integer = 0
        Dim bnum As Integer = 0

        For tc = 0 To ds.Tables.Count - 1
            If ds.Tables(tc).Rows.Count > 0 Then
                acct = ds.Tables(tc).Rows(0).Item("Account")
                Dim ytd As Boolean = False
                If htBBNames_ACCT.ContainsKey(acct) Then
                    acct = htBBNames_ACCT(acct)
                End If
                If IsDBNull(ds.Tables(tc).Rows(0).Item("YTD")) Then
                    a_y = ds.Tables(tc).Rows(0).Item("ITD")
                    ytd = False
                Else
                    a_y = ds.Tables(tc).Rows(0).Item("YTD")
                    ytd = True
                End If

                ''write account line
                readBob &= "<tr><B>" &
                            "<td WIDTH=150 ALIGN=left style=white-space: wrap bgcolor=#D3D3D3> " & acct & "</td>"

                If a_y < 0.0 Then
                    readBob &= "<td WIDTH=55 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_y) & "</font></td>"
                Else
                    readBob &= "<td WIDTH=55 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_y & "</font></td>"
                End If


                readBob &= "</B></tr>"

                numbench = ds.Tables(tc).Rows.Count - 1
                For bnum = 1 To numbench
                    B = ds.Tables(tc).Rows(bnum).Item("Account")
                    If htBBNames_BNCH.ContainsKey(B) Then
                        B = htBBNames_BNCH(B)
                    End If
                    If ytd = True Then
                        b_y = ds.Tables(tc).Rows(bnum).Item("YTD")
                    Else
                        b_y = ds.Tables(tc).Rows(bnum).Item("ITD")
                    End If

                    ''write benchmark line
                    readBob &= "<tr><B>" &
                                "<td WIDTH=150 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3> " & B & "</td>"

                    If b_y < 0.0 Then
                        readBob &= "<td WIDTH=55 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(b_y) & "</font></td>"
                    Else
                        readBob &= "<td WIDTH=55 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & b_y & "</font></td>"
                    End If

                    readBob &= "</B></tr>"

                    ''write diff line
                    readBob &= "<tr><B>" &
                                "<td WIDTH=150 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3>diff</td>"

                    If a_y - b_y < 0.0 Then
                        readBob &= "<td WIDTH=55 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_y - b_y) & "</font></td>"
                    Else
                        readBob &= "<td WIDTH=55 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_y - b_y & "</font></td>"
                    End If

                    readBob &= "</B></tr>"
                Next

                readBob &= "		<tr bgcolor=#000000><td colspan=2></td></tr>"
            End If
        Next


        readBob &= "	</FONT></table>" &
                            "</body>" &
                            "</html>"

        If emaillist = "" Then emaillist = "BOB"
        SendEmail(rundate & " AM Performance - YTD", emaillist, "", readBob)

    End Sub

    Private Sub Email_Blackberry(ByVal msg As String, ByVal emaillist As String)
        Dim readBB As String = ""

        readBB = msg & vbCrLf & "<html>" &
                            "<head>" &
                            "<title>BlackBerry Performance</title>" &
                            "</head>" &
                            "<body>" &
                            "	<table rules=none border=1 frame=box><FONT SIZE=1>" &
                            "		<tr><td colspan=5><B>" &
                            "				<Label id=verify>AM Perf Numbers * Gross</Label>" &
                            "		</B></td></tr>" &
                            "		<tr><td colspan=5><B>" &
                            "				<Label id=verify>" & rundate & "</Label>" &
                            "		</B></td></tr>"

        readBB &= "		<tr bgcolor=#87CEFA><B>" &
                          "<td WIDTH=75 ALIGN=left style=white-space: wrap>ACCOUNT</td>" &
                          "<td WIDTH=45 ALIGN=center style=white-space: nowrap>DTD</td>" &
                          "<td WIDTH=45 ALIGN=center style=white-space: nowrap>MTD</td>" &
                          "<td WIDTH=45 ALIGN=center style=white-space: nowrap>QTD</td>" &
                          "<td WIDTH=45 ALIGN=center style=white-space: nowrap>YTD</td>" &
                          "</B></tr>" &
                 "		<tr bgcolor=#000000><td colspan=5></td></tr>"


        Dim acct As String = ""
        Dim dtd As Boolean = False
        Dim mtd As Boolean = False
        Dim qtd As Boolean = False
        Dim ytd As Boolean = False
        Dim a_d As Decimal = 0.0
        Dim a_m As Decimal = 0.0
        Dim a_q As Decimal = 0.0
        Dim a_y As Decimal = 0.0
        Dim a_i As Decimal = 0.0
        Dim B As String = ""
        Dim b_d As Decimal = 0.0
        Dim b_m As Decimal = 0.0
        Dim b_q As Decimal = 0.0
        Dim b_y As Decimal = 0.0
        Dim b_i As Decimal = 0.0

        Dim getdata As DataAccess = New DataAccess
        Dim ds As DataSet = New DataSet
        ds = getdata.GetPerformance(connPM, rundate, "no")
        Dim tc As Integer = 0
        Dim numbench As Integer = 0
        Dim bnum As Integer = 0

        For tc = 0 To ds.Tables.Count - 1
            If ds.Tables(tc).Rows.Count > 0 Then
                acct = ds.Tables(tc).Rows(0).Item("Account")
                dtd = False
                mtd = False
                qtd = False
                ytd = False

                a_i = ds.Tables(tc).Rows(0).Item("ITD")
                If htBBNames_ACCT.ContainsKey(acct) Then
                    acct = htBBNames_ACCT(acct)
                End If
                a_d = Round(ds.Tables(tc).Rows(0).Item("DTD"), 2)
                dtd = True

                If IsDBNull(ds.Tables(tc).Rows(0).Item("MTD")) Then
                    a_m = a_i
                    mtd = False
                    'getdata.ImportPerformance(rundate, acct, curr, "MTD", a_m)
                Else
                    a_m = ds.Tables(tc).Rows(0).Item("MTD")
                    mtd = True
                End If

                If IsDBNull(ds.Tables(tc).Rows(0).Item("QTD")) Then
                    a_q = a_i
                    qtd = False
                    'getdata.ImportPerformance(rundate, acct, curr, "QTD", a_q)
                Else
                    a_q = ds.Tables(tc).Rows(0).Item("QTD")
                    qtd = True
                End If

                If IsDBNull(ds.Tables(tc).Rows(0).Item("YTD")) Then
                    a_y = a_i
                    ytd = False
                    'getdata.ImportPerformance(rundate, acct, curr, "YTD", a_y)
                Else
                    a_y = ds.Tables(tc).Rows(0).Item("YTD")
                    ytd = True
                End If

                ''write account line
                readBB &= "<tr><B>" &
                            "<td WIDTH=80 ALIGN=left style=white-space: wrap bgcolor=#D3D3D3> " & acct & "</td>"

                If a_d < 0.0 Then
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_d) & "</font></td>"
                Else
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_d & "</font></td>"
                End If

                If a_m < 0.0 Then
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_m) & "</font></td>"
                Else
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_m & "</font></td>"
                End If

                If a_q < 0.0 Then
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_q) & "</font></td>"
                Else
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_q & "</font></td>"
                End If

                If a_y < 0.0 Then
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_y) & "</font></td>"
                Else
                    readBB &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_y & "</font></td>"
                End If


                readBB &= "</B></tr>"

                numbench = ds.Tables(tc).Rows.Count - 1
                For bnum = 1 To numbench
                    B = ds.Tables(tc).Rows(bnum).Item("Account")
                    If htBBNames_BNCH.ContainsKey(B) Then
                        B = htBBNames_BNCH(B)
                    End If
                    b_d = ds.Tables(tc).Rows(bnum).Item("DTD")
                    b_i = ds.Tables(tc).Rows(bnum).Item("ITD")

                    If mtd = True Then
                        If IsDBNull(ds.Tables(tc).Rows(bnum).Item("MTD")) Then
                            b_m = b_d
                        Else
                            b_m = ds.Tables(tc).Rows(bnum).Item("MTD")
                        End If
                    Else
                        b_m = b_i
                    End If

                    If qtd = True Then
                        If IsDBNull(ds.Tables(tc).Rows(bnum).Item("QTD")) Then
                            b_q = b_m
                        Else
                            b_q = ds.Tables(tc).Rows(bnum).Item("QTD")
                        End If
                    Else
                        b_q = b_i
                    End If

                    If ytd = True Then
                        If IsDBNull(ds.Tables(tc).Rows(bnum).Item("YTD")) Then
                            b_y = b_q
                        Else
                            b_y = ds.Tables(tc).Rows(bnum).Item("YTD")
                        End If
                    Else
                        b_y = b_i
                    End If

                    ''write benchmark line
                    readBB &= "<tr><B>" &
                                "<td WIDTH=80 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3> " & B & "</td>"

                    If b_d < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(b_d) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & b_d & "</font></td>"
                    End If

                    If b_m < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(b_m) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & b_m & "</font></td>"
                    End If

                    If b_q < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(b_q) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & b_q & "</font></td>"
                    End If

                    If b_y < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(b_y) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & b_y & "</font></td>"
                    End If

                    readBB &= "</B></tr>"

                    ''write diff line
                    readBB &= "<tr><B>" &
                                "<td WIDTH=80 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3>diff</td>"

                    If a_d - b_d < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_d - b_d) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_d - b_d & "</font></td>"
                    End If

                    If a_m - b_m < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_m - b_m) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_m - b_m & "</font></td>"
                    End If

                    If a_q - b_q < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_q - b_q) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_q - b_q & "</font></td>"
                    End If

                    If a_y - b_y < 0.0 Then
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_y - b_y) & "</font></td>"
                    Else
                        readBB &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_y - b_y & "</font></td>"
                    End If

                    readBB &= "</B></tr>"
                Next

                readBB &= "		<tr bgcolor=#000000><td colspan=5></td></tr>"
            End If
        Next


        readBB &= "	</FONT></table>" &
                            "</body>" &
                            "</html>"

        If emaillist = "" Then emaillist = "BB"
        SendEmail(rundate & " AM Performance Blackberry", emaillist, "", readBB)

    End Sub

    Private Sub Email_130(ByVal msg As String, ByVal emaillist As String)
        Dim read130 As String = ""

        read130 &= msg & vbCrLf & "<html>" &
                            "<head>" &
                            "<title>130-30 Performance</title>" &
                            "</head>" &
                            "<body>" &
                            "	<table rules=none border=1 frame=box><FONT SIZE=2>"

        read130 &= "<tr bgcolor=#87CEFA><B>" &
                          "<td WIDTH=70 ALIGN=middle style=white-space: wrap>" & rundate.ToString("ddd, MMM d, yyyy") & "</td>" &
                          "<td WIDTH=15 ALIGN=middle style=white-space: nowrap>DTD</td>" &
                          "<td WIDTH=15 ALIGN=middle style=white-space: nowrap>MTD</td>" &
                          "<td WIDTH=15 ALIGN=middle style=white-space: nowrap>QTD</td>" &
                          "<td WIDTH=15 ALIGN=middle style=white-space: nowrap>YTD</td>" &
                    "</B></tr>"



        Dim acct As String = ""
        Dim a_d As Decimal = 0.0
        Dim a_m As Decimal = 0.0
        Dim a_q As Decimal = 0.0
        Dim a_y As Decimal = 0.0
        Dim B As String = ""
        Dim b_d As Decimal = 0.0
        Dim b_m As Decimal = 0.0
        Dim b_q As Decimal = 0.0
        Dim b_y As Decimal = 0.0

        Dim getdata As DataAccess = New DataAccess
        Dim ds As DataSet = New DataSet
        ds = getdata.GetPerformance(connPM, rundate, "yes")
        Dim tc As Integer = 0
        Dim numbench As Integer = 0
        Dim bnum As Integer = 0

        For tc = 0 To ds.Tables.Count - 1
            If tc = 0 Then
                read130 &= "<tr><td colspan=5 ALIGN=left bgcolor=#AFC7C7><B>Net</B></td></tr>"
            ElseIf tc = 1 Then
                read130 &= "<tr><td colspan=5 ALIGN=left bgcolor=#AFC7C7><B>Long</B></td></tr>"
            ElseIf tc = 2 Then
                read130 &= "<tr><td colspan=5 ALIGN=left bgcolor=#AFC7C7><B>Short</B></td></tr>"
            End If

            acct = Replace(Replace(Replace(ds.Tables(tc).Rows(0).Item("Account"), " Net", ""), " Long", ""), " Short", "")
            a_d = ds.Tables(tc).Rows(0).Item("DTD")
            a_m = ds.Tables(tc).Rows(0).Item("MTD")
            a_q = ds.Tables(tc).Rows(0).Item("QTD")
            a_y = ds.Tables(tc).Rows(0).Item("YTD")


            ''write account line
            read130 &= "<tr><B>" &
                        "<td WIDTH=80 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3> " & acct & "</td>"

            If a_d < 0.0 Then
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_d) & "</font></td>"
            Else
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_d & "</font></td>"
            End If

            If a_m < 0.0 Then
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_m) & "</font></td>"
            Else
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_m & "</font></td>"
            End If

            If a_q < 0.0 Then
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_q) & "</font></td>"
            Else
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_q & "</font></td>"
            End If

            If a_y < 0.0 Then
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_y) & "</font></td>"
            Else
                read130 &= "<td WIDTH=45 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_y & "</font></td>"
            End If


            read130 &= "</B></tr>"

            numbench = ds.Tables(tc).Rows.Count - 1
            For bnum = 1 To numbench
                B = ds.Tables(tc).Rows(bnum).Item("Account")
                If tc = 1 Then
                    b_d = Round(ds.Tables(tc).Rows(bnum).Item("DTD") * 1.3, 2)
                    b_m = Round(ds.Tables(tc).Rows(bnum).Item("MTD") * 1.3, 2)
                    b_q = Round(ds.Tables(tc).Rows(bnum).Item("QTD") * 1.3, 2)
                    b_y = Round(ds.Tables(tc).Rows(bnum).Item("YTD") * 1.3, 2)
                ElseIf tc = 2 Then
                    b_d = Round(ds.Tables(tc).Rows(bnum).Item("DTD") * -0.3, 2)
                    b_m = Round(ds.Tables(tc).Rows(bnum).Item("MTD") * -0.3, 2)
                    b_q = Round(ds.Tables(tc).Rows(bnum).Item("QTD") * -0.3, 2)
                    b_y = Round(ds.Tables(tc).Rows(bnum).Item("YTD") * -0.3, 2)
                Else
                    b_d = ds.Tables(tc).Rows(bnum).Item("DTD")
                    b_m = ds.Tables(tc).Rows(bnum).Item("MTD")
                    b_q = ds.Tables(tc).Rows(bnum).Item("QTD")
                    b_y = ds.Tables(tc).Rows(bnum).Item("YTD")
                End If

                ''write benchmark line
                read130 &= "<tr><B>" &
                            "<td WIDTH=80 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3> " & B & "</td>"

                If b_d < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(b_d) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & b_d & "</font></td>"
                End If

                If b_m < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(b_m) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & b_m & "</font></td>"
                End If

                If b_q < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(b_q) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & b_q & "</font></td>"
                End If

                If b_y < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(b_y) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & b_y & "</font></td>"
                End If

                read130 &= "</B></tr>"

                ''write diff line
                read130 &= "<tr><B>" &
                            "<td WIDTH=80 ALIGN=right style=white-space: wrap bgcolor=#D3D3D3>diff</td>"

                If a_d - b_d < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_d - b_d) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_d - b_d & "</font></td>"
                End If

                If a_m - b_m < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_m - b_m) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_m - b_m & "</font></td>"
                End If

                If a_q - b_q < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=red>" & Math.Abs(a_q - b_q) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#C2DFFF><font color=green>" & a_q - b_q & "</font></td>"
                End If

                If a_y - b_y < 0.0 Then
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=red>" & Math.Abs(a_y - b_y) & "</font></td>"
                Else
                    read130 &= "<td WIDTH=35 ALIGN=right style=white-space: nowrap bgcolor=#B4CFEC><font color=green>" & a_y - b_y & "</font></td>"
                End If

                read130 &= "</B></tr>"
            Next

        Next

        read130 &= "<tr><td colspan=5 ALIGN=left bgcolor=#AFC7C7><B>For internal use only.</B></td></tr>"
        read130 &= "<tr><td colspan=5 ALIGN=left bgcolor=#AFC7C7><B>May not be redistributed without Compliance approval.</B></td></tr>"
        read130 &= "	</FONT></table>" &
                            "</body>" &
                            "</html>"


        If emaillist = "" Then emaillist = "130"
        SendEmail(rundate & " AM Performance 130-30", emaillist, "", read130)

    End Sub

    Private Sub SendEmail(ByVal subject As String, ByVal emaillist As String, ByVal xlsfile As String, ByVal bodytext As String)
        Dim reciplist As String = ""
        Dim cclist As String = ""

        'If Me.cbxTest.Checked = False Then
        Dim objTR As IO.TextReader
        objTR = IO.File.OpenText(refpath & "EmailRecipients.txt")
        Dim email As String = ""
        Dim ct As Integer = 0
        Dim readline As String = ""
        While objTR.Peek <> -1
            readline = objTR.ReadLine()
            If Len(readline) > 0 Then
                Dim splLine() As String = Split(readline, vbTab)
                'Debug.WriteLine(splLine(0))
                If splLine(0) = emaillist Then
                    email = splLine(1)
                    reciplist &= email & "; "
                End If
            End If
        End While
        If emaillist = "TEST" Then subject = "Test: " & subject
        'Else
        'reciplist = "jcarleton@mckinleycapital.com; mgose@mckinleycapital.com"
        'subject = "Test: " & subject
        'End If

        If reciplist.Length > 0 Then
            If reciplist.Substring(Len(reciplist) - 2, 2) = "; " Then
                reciplist = reciplist.Substring(0, Len(reciplist) - 2)
            End If

            ' Create a new MailItem.     
            Dim oApp As Outlook.Application = New Outlook.Application()
            Dim oMail As MailItem = oApp.CreateItem(OlItemType.olMailItem)
            oMail.To = reciplist
            oMail.Subject = subject
            If xlsfile <> "" Then
                oMail.Attachments.Add(xlsfile)
            End If
            oMail.BodyFormat = OlBodyFormat.olFormatHTML

            oMail.HTMLBody = bodytext

            oMail.Send()
            oApp = Nothing
            oMail = Nothing
        End If
    End Sub

#End Region

#Region "Menu Items"

    Private Sub mnuUpdateDisplay_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuUpdDisplay.Click
        Me.txtProgress.Text &= vbCrLf & "Updating display list."
        ScrollToBottom()
        RefreshApp()
        Dim getdata As DataAccess = New DataAccess
        getdata.DeleteDisplayList(connPM)

        Dim XLSAppDISPLAY As New Excel.Application
        Dim WkBkDISPLAY As Workbook = XLSAppDISPLAY.Workbooks.Open(refpath & displayfile)
        Dim WkShtDISPLAY As Worksheet = CType(WkBkDISPLAY.Worksheets(1), Worksheet)

        Dim maxrow As Integer = WkShtDISPLAY.UsedRange.Count

        Dim rundate As Date = rundate
        Dim startrow As Integer = 2

        Dim row As Integer = 4
        Dim col As Integer = 0
        Dim newcol As String = ""
        Dim listorder As Integer = 0
        Dim acct As String = ""
        Dim curr As String = ""
        Dim inc As Date
        Dim b1 As String = ""
        Dim b2 As String = ""
        Dim b3 As String = ""


        For row = 2 To maxrow
            b1 = ""
            b2 = ""
            b3 = ""
            inc = Nothing

            newcol = WkShtDISPLAY.Cells(row, 1).value
            If newcol <> col Then
                listorder = 1
            End If
            If newcol = 3 Then
                listorder = 0
            End If

            col = WkShtDISPLAY.Cells(row, 1).value
            acct = RTrim(LTrim(WkShtDISPLAY.Cells(row, 2).value))
            If acct <> Nothing Then
                Select Case acct.Substring(Len(acct) - 4, 3)
                    Case "USD"
                        curr = "USD"
                    Case "CAD"
                        curr = "CAD"
                    Case "EUR"
                        curr = "EUR"
                    Case "JPY"
                        curr = "JPY"
                    Case "GBP"
                        curr = "GBP"
                    Case Else
                        curr = "USD"
                End Select

                If Convert.ToString(WkShtDISPLAY.Cells(row, 3).value) <> "" Then
                    inc = Convert.ToDateTime(WkShtDISPLAY.Cells(row, 3).value)
                Else
                    inc = "1/1/1900"
                End If



                If WkShtDISPLAY.Cells(row, 4).value <> "" Then
                    b1 = Replace(Replace(Replace(Replace(WkShtDISPLAY.Cells(row, 4).value, " (JPY)", ""), " (CAD)", ""), " (EUR)", ""), " (GBP)", "")
                    b1 = RTrim(LTrim(b1))
                End If
                If WkShtDISPLAY.Cells(row, 5).value <> "" Then
                    b2 = Replace(Replace(Replace(Replace(WkShtDISPLAY.Cells(row, 5).value, " (JPY)", ""), " (CAD)", ""), " (EUR)", ""), " (GBP)", "")
                    b2 = RTrim(LTrim(b2))
                End If
                If WkShtDISPLAY.Cells(row, 6).value <> "" Then
                    b3 = Replace(Replace(Replace(Replace(WkShtDISPLAY.Cells(row, 6).value, " (JPY)", ""), " (CAD)", ""), " (EUR)", ""), " (GBP)", "")
                    b3 = RTrim(LTrim(b3))
                End If

                getdata.UpdateDisplayList(connPM, acct, inc, curr, b1, b2, b3, col, listorder)
                listorder += 1

            End If
        Next

        WkBkDISPLAY.Close()
        XLSAppDISPLAY = Nothing

        LoadAccountList()

        Me.txtProgress.Text &= vbCrLf & vbTab & "Display list updated."
        ScrollToBottom()
        RefreshApp()
    End Sub

    Private Sub mnuOpenFile_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuOpenFile.Click
        Process.Start(refpath & displayfile)
    End Sub

    Private Sub mnuUpdateAccount_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuUpdateAccount.Click
        If aryUpdDate Is Nothing = False Then
            updDate = aryUpdDate(0)
        End If
        If updDate = Nothing Then
            updDate = MTDFrom
        End If
        Dim Update As UpdateAccount = New UpdateAccount(dtRerunGroups, repfolder, updDate)
        Update.ConnectionPM = connPM
        Update.ShowDialog()
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuAbout.Click
        Dim abt As About = About
        abt.ShowDialog()
    End Sub

    Private Sub mnuValidPerf_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuValidPerf.Click
        Dim val As Validation = New Validation(rundate)
        val.ConnectionPM = connPM
        val.Show()
    End Sub

    Private Sub mnuIndex_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuIndex.Click
        Dim getdata As DataAccess = New DataAccess
        Dim dtMissDex As Data.DataTable = New Data.DataTable
        Dim x As Integer = 0
        Dim index As String = ""
        Dim missing As String = ""
        dtMissDex = getdata.GetMissingIndexPrices(connPM, rundate)
        If dtMissDex.Rows.Count > 0 Then
            Me.txtProgress.Text &= vbCrLf & "Missing index prices: "
            For x = 0 To dtMissDex.Rows.Count - 1
                index = dtMissDex.Rows(x).Item("IndexName")
                Me.txtProgress.Text &= vbCrLf & vbTab & index
            Next
        Else
            Me.txtProgress.Text &= vbCrLf & "Index prices are up-to-date for " & rundate
        End If
        Me.ScrollToBottom()
    End Sub

    Private Sub mnuStartDates_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuStartDates.Click
        Dim ManageStartDates As StartDates = New StartDates
        ManageStartDates.ConnectionPM = connPM
        ManageStartDates.ShowDialog()
    End Sub

#End Region

    'Private Sub ProcessReportFiles()

    '    Me.txtProgress.Text &= vbCrLf & "Importing performance"
    '    ScrollToBottom()
    '    RefreshApp()

    '    Dim c As Integer = 0
    '    Dim filecurr As String = ""
    '    Dim readcell1 As String = ""
    '    Dim readcell2 As String = ""
    '    Dim readYTD As String = ""
    '    Dim readrow As Integer = 1
    '    Dim maxrow As Integer = 1
    '    Dim shortname As String = ""
    '    Dim fullname As String = ""
    '    Dim benchname As String = ""
    '    Dim acctperfrow As Integer = 1

    '    Dim b_dtd As String = ""
    '    Dim b_mtd As String = ""
    '    Dim b_qtd As String = ""
    '    Dim b_ytd As String = ""
    '    Dim b_itd As String = ""

    '    Dim dtd As Boolean = False
    '    Dim mtd As Boolean = False
    '    Dim qtd As Boolean = False
    '    Dim ytd As Boolean = False

    '    Dim dtdcol As Integer = 2
    '    Dim mtdcol As Integer = 3
    '    Dim qtdcol As Integer = 4
    '    Dim ytdcol As Integer = 5
    '    Dim itdcol As Integer = 6
    '    Dim incept As Date
    '    Dim STRincept As String = ""

    '    Dim a_d, a_m, a_q, a_y, a_i As Decimal
    '    Dim b1_d, b1_m, b1_q, b1_y, b1_i As Decimal

    '    Dim labelrow As Integer = 1
    '    Dim labelcol As Integer = 1
    '    Dim col As Integer = 0

    '    Dim htMissPrice As Hashtable = New Hashtable

    '    Me.txtProgress.Text &= vbCrLf & "Missing Prices:"
    '    Dim perflist As String = ""

    '    Dim XLSAppLOAD As New Microsoft.Office.Interop.Excel.Application

    '    For c = 0 To cur.GetUpperBound(0)
    '        filecurr = cur(c)
    '        Dim WkBkLOAD As Microsoft.Office.Interop.Excel.Workbook = XLSAppLOAD.Workbooks.Open(datafilepath & "PRFMc_" & filecurr & ".xlsx")
    '        Dim WkShtLOAD As Microsoft.Office.Interop.Excel.Worksheet = CType(WkBkLOAD.Worksheets(1), Microsoft.Office.Interop.Excel.Worksheet)

    '        maxrow = WkShtLOAD.UsedRange.Count

    '        For readrow = 1 To maxrow
    '            readcell1 = WkShtLOAD.Cells(readrow, 1).value
    '            readcell2 = WkShtLOAD.Cells(readrow, 2).value

    '            If readcell1 <> Nothing Then
    '                If readcell1 <> "" And readcell2 = "" And readcell1.Contains("For the Period Ended") = False Then
    '                    fullname = readcell1
    '                    shortname = Replace(Replace(Replace(Replace(readcell1, " (DTD)", ""), " (MTD)", ""), " (QTD)", ""), " (YTD)", "")
    '                    dtd = False
    '                    mtd = False
    '                    qtd = False
    '                    ytd = False

    '                    If htAcctList.ContainsKey(shortname.ToUpper) = True Then
    '                        acctperfrow = readrow + 5
    '                        If shortname = "NASDAQ COMP" Then
    '                            acctperfrow += 1
    '                        End If
    '                        labelrow = readrow + 2

    '                        For col = 2 To WkShtLOAD.UsedRange.Columns.Count
    '                            If WkShtLOAD.Cells(labelrow, col).value = "Date" Then
    '                                dtdcol = col
    '                                dtd = True
    '                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Month" Then
    '                                mtdcol = col
    '                                mtd = True
    '                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Quarter" Then
    '                                qtdcol = col
    '                                qtd = True
    '                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Year" Then
    '                                ytdcol = col
    '                                ytd = True
    '                            ElseIf WkShtLOAD.Cells(labelrow, col).value = "Inception" Then
    '                                itdcol = col
    '                                incept = WkShtLOAD.Cells(labelrow + 1, itdcol).value
    '                                STRincept = incept.ToString("MMddyyyy")
    '                            End If

    '                        Next

    '                        If fullname.Contains("DTD") Then
    '                            If dtd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, dtdcol).value) <> "?" Then
    '                                    a_d = WkShtLOAD.Cells(acctperfrow, dtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "DTD", a_d, incept)
    '                                End If
    '                            End If
    '                        ElseIf fullname.Contains("MTD") Then
    '                            If mtd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, mtdcol).value) <> "?" Then
    '                                    a_m = WkShtLOAD.Cells(acctperfrow, mtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "MTD", a_m, incept)
    '                                End If
    '                            End If
    '                        ElseIf fullname.Contains("QTD") Then
    '                            If qtd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, qtdcol).value) <> "?" Then
    '                                    a_q = WkShtLOAD.Cells(acctperfrow, qtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "QTD", a_q, incept)
    '                                End If
    '                            End If
    '                        ElseIf fullname.Contains("YTD") Then
    '                            If ytd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, ytdcol).value) <> "?" Then
    '                                    a_y = WkShtLOAD.Cells(acctperfrow, ytdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "YTD", a_y, incept)
    '                                End If
    '                            End If

    '                            If Convert.ToString(WkShtLOAD.Cells(acctperfrow, itdcol).value) <> "?" Then
    '                                a_i = WkShtLOAD.Cells(acctperfrow, itdcol).value
    '                                getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "ITD", a_i, incept)
    '                            End If
    '                        Else
    '                            If dtd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, dtdcol).value) <> "?" Then
    '                                    a_d = WkShtLOAD.Cells(acctperfrow, dtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "DTD", a_d, incept)
    '                                End If
    '                            End If
    '                            If mtd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, mtdcol).value) <> "?" Then
    '                                    a_m = WkShtLOAD.Cells(acctperfrow, mtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "MTD", a_m, incept)
    '                                End If
    '                            End If
    '                            If qtd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, qtdcol).value) <> "?" Then
    '                                    a_q = WkShtLOAD.Cells(acctperfrow, qtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "QTD", a_q, incept)
    '                                End If
    '                            End If
    '                            If ytd = True Then
    '                                If Convert.ToString(WkShtLOAD.Cells(acctperfrow, ytdcol).value) <> "?" Then
    '                                    a_y = WkShtLOAD.Cells(acctperfrow, ytdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "YTD", a_y, incept)
    '                                End If
    '                            End If
    '                            If Convert.ToString(WkShtLOAD.Cells(acctperfrow, itdcol).value) <> "?" Then
    '                                a_i = WkShtLOAD.Cells(acctperfrow, itdcol).value
    '                                getdata.ImportPerformance(connPM, rundate, shortname, filecurr, "ITD", a_i, incept)
    '                            End If
    '                        End If

    '                        perflist &= fullname & vbTab & incept & vbTab & a_d & vbTab & a_m & vbTab & a_q & vbTab & a_y & vbTab & a_i & vbCrLf


    '                    ElseIf readcell1 <> "" And readcell2 <> "" And readcell1 <> "Account" And readcell1 <> "Difference (Gross)" Then
    '                        benchname = readcell1

    '                        If Convert.ToString(WkShtLOAD.Cells(readrow, dtdcol).value) = "?" Or Convert.ToString(WkShtLOAD.Cells(readrow, mtdcol).value) = "?" Or Convert.ToString(WkShtLOAD.Cells(readrow, qtdcol).value) = "?" Or Convert.ToString(WkShtLOAD.Cells(readrow, ytdcol).value) = "?" Then
    '                            If htMissPrice.ContainsKey(benchname) = False Then
    '                                Me.txtProgress.Text &= vbCrLf & benchname
    '                                htMissPrice.Add(benchname, benchname)
    '                            End If

    '                        Else

    '                            If Not getdata.PerfDataExists(connPM, rundate, benchname, filecurr, incept) Then
    '                                If dtd = True Then
    '                                    b1_d = WkShtLOAD.Cells(readrow, dtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "DTD", b1_d, incept)
    '                                Else
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "DTD", Nothing, incept)
    '                                End If

    '                                If mtd = True Then
    '                                    b1_m = WkShtLOAD.Cells(readrow, mtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "MTD", b1_m, incept)
    '                                Else
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "MTD", Nothing, incept)
    '                                End If

    '                                If qtd = True Then
    '                                    b1_q = WkShtLOAD.Cells(readrow, qtdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "QTD", b1_q, incept)
    '                                Else
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "QTD", Nothing, incept)
    '                                End If

    '                                If ytd = True Then
    '                                    b1_y = WkShtLOAD.Cells(readrow, ytdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "YTD", b1_y, incept)
    '                                Else
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "YTD", Nothing, incept)
    '                                End If

    '                                If Convert.ToString(WkShtLOAD.Cells(readrow, itdcol).value) <> "?" Then
    '                                    b1_i = WkShtLOAD.Cells(readrow, itdcol).value
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "ITD", b1_i, incept)
    '                                Else
    '                                    getdata.ImportPerformance(connPM, rundate, benchname, filecurr, "ITD", Nothing, incept)
    '                                End If

    '                            End If
    '                        End If
    '                        perflist &= fullname & vbTab & incept & vbTab & b1_d & vbTab & b1_m & vbTab & b1_q & vbTab & b1_y & vbTab & b1_i & vbCrLf
    '                    End If

    '                End If
    '            End If
    '        Next
    '        WkBkLOAD.Close()
    '    Next

    '    Debug.Write(perflist)

    '    XLSAppLOAD = Nothing
    '    Me.txtProgress.Text &= vbTab & vbCrLf & "Import complete."
    '    ScrollToBottom()
    '    RefreshApp()
    'End Sub

End Class


