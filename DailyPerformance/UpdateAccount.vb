Public Class UpdateAccount

#Region "Variables"
    Dim getdata As DataAccess = New DataAccess
    Dim rundateSTR As String = ""
    Dim deletedate As Date
    Dim updatedate As Date
    Dim grp As String
    Dim dailyperfpath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\OttoUpdateFiles\"
    Dim updatepath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\MorningUpdates\"
    Dim datafilepath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\DataFiles\"
    Dim logdate As String = DateAdd(DateInterval.Day, -1, Today()).ToString("MMddyy")
    Dim loginfo As String = ""
    Dim updLog As String = updatepath & "UpdatePerf " & logdate & ".log"
    Dim objLOG As IO.TextWriter

    Dim objDir As IO.DirectoryInfo
    Dim objFileI As IO.FileInfo
    Dim objFiles() As IO.FileInfo
#End Region

#Region "Properties"

    Dim connPM As String

    Public Property ConnectionPM() As String
        Get
            Return connPM
        End Get
        Set(ByVal Value As String)
            connPM = Value
        End Set
    End Property

    Public Property dtRerunGrps() As Data.DataTable

    Public Property rundate() As Date

    Public Property reppath() As String

    Public Sub New(ByVal dt As Data.DataTable, ByVal path As String, ByVal rdate As Date)
        MyBase.New()
        InitializeComponent()
        dtRerunGrps = dt
        reppath = path
        rundate = rdate
    End Sub

#End Region

    Private Sub UpdateAccount_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        objLOG = IO.File.CreateText(updLog)
        objLOG.WriteLine("UPDATE: " & Now())

        Dim dtUpdGrps As Data.DataTable = New Data.DataTable
        Dim getdata As DataAccess = New DataAccess
        dtUpdGrps = getdata.GetPRFMGroups(connPM)

        Dim x As Integer

        For x = 0 To dtUpdGrps.Rows.Count - 1
            grp = dtUpdGrps.Rows(x).Item("Name")

            Select Case grp
                Case "PRFM_USD", "PRFM_CAD", "PRFM_GBP", "PRFM_EUR", "PRFM_JPY", "PRFM_130", "PRFM_UPDATE"
                    grp = "@" & grp
                Case Else
                    grp = "+@" & grp
            End Select
            Me.lbxPerfGrps.Items.Add(grp)
        Next


        For x = 0 To dtRerunGrps.Rows.Count - 1
            Me.lbxUpdAccts.Items.Add("+@" & dtRerunGrps.Rows(x).Item("Name"))
        Next

        Me.dtpFrom.Value = rundate
        Me.dtpTo.Value = DateAdd(DateInterval.Day, -1, Today())
        rundateSTR = rundate.ToString("MMddyy")
    End Sub

    Private Sub lbxUpdAccts_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxUpdAccts.DoubleClick
        If lbxUpdAccts.SelectedIndex <> -1 Then
            lbxPerfGrps.Items.Add(lbxUpdAccts.SelectedItem())
            lbxUpdAccts.Items.RemoveAt(lbxUpdAccts.SelectedIndex)
        End If
    End Sub

    Private Sub lbxPerfGrps_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbxPerfGrps.DoubleClick
        If lbxPerfGrps.SelectedIndex <> -1 Then
            lbxUpdAccts.Items.Add(lbxPerfGrps.SelectedItem())
            lbxPerfGrps.Items.RemoveAt(lbxPerfGrps.SelectedIndex)
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
       Form1.txtProgress.Text = "UPDATE: " & Now()

        Dim getdata As DataAccess = New DataAccess
        Dim grp As String = ""
        Dim curr As String = ""
        Dim deletefiles As String


        Dim objFile As IO.FileInfo
        Dim objFiles() As IO.FileInfo
        Dim objDir As IO.DirectoryInfo
        objDir = New IO.DirectoryInfo(datafilepath)

        deletefiles = "*_Update*.xls"
        objFiles = objDir.GetFiles(deletefiles)
        For Each objFile In objFiles
            objFile.Delete()
        Next

        deletefiles = "*_Delete*.xls"
        objFiles = objDir.GetFiles(deletefiles)
        For Each objFile In objFiles
            objFile.Delete()
        Next


        Dim q As Integer = 0
        For q = 0 To Me.lbxUpdAccts.Items.Count - 1

            Dim sdate As Date = Me.dtpFrom.Value
            Dim sdateSTR As String = sdate.ToString("MMddyy")
            Dim edate As Date = Me.dtpTo.Value
            Dim edateSTR As String = edate.ToString("MMddyy")
            Dim deletedateSTR As String = DateAdd(DateInterval.Day, 1, sdate).ToString("MMddyy")

            grp = Me.lbxUpdAccts.Items(q)
            lblAcct.Text = grp

            Select Case grp.ToUpper
                Case "+@PRFMB_DGFBMCG1", "+@PRFMA_RUSSBZ9", "@PRFM_EUR"
                    curr = "EUR"
                Case "+@PRFMA_CATUK9", "+@PRFMA_MODXUK2", "+@PRFMA_UKMOM9", "@PRFM_GBP"
                    curr = "GBP"
                Case "+@PRFMA_RUSREIT9", "@PRFM_JPY"
                    curr = "JPY"
                Case "@PRFM_CAD"
                    curr = "CAD"
                Case Else
                    curr = "USD"
            End Select


            Dim x As Integer = DatePart(DateInterval.Month, sdate)
            Dim y As Integer = DatePart(DateInterval.Month, DateAdd(DateInterval.Day, -1, Today()))
            Dim z As Integer = DatePart(DateInterval.Year, sdate)
            Dim o As Integer = DatePart(DateInterval.Year, Today())



            loginfo &= Form1.CreateBatFile("PRFM_Delete" & curr & ".mac", grp, curr, deletedateSTR, edateSTR, "", reppath)
            objLOG.WriteLine(grp & " - Deleted: " & deletedateSTR & " - " & edateSTR)

            loginfo &= Form1.CreateBatFile("PRFM_UpdateDay" & curr & ".mac", grp, curr, sdateSTR, edateSTR, "", reppath)
            objLOG.WriteLine(grp & " - Updated Daily: " & sdateSTR & " - " & edateSTR)

            lblAcct.Text = grp & " done."
        Next

        CheckUpdateStatus(curr)
        lblAcct.Text = "Done!"
        objLOG.Close()
    End Sub

    Public Sub CheckUpdateStatus(ByVal curr As String)

        Dim XLSApp As New Microsoft.Office.Interop.Excel.Application
        Dim WkBk As Workbook
        Dim WkSht As Worksheet

        Dim misslist As String = ""
        Dim missct As Integer = 0
        Dim failurelist As String = ""
        Dim successlist As String = ""
        Dim failurect As Integer = 0
        Dim htMiss As Hashtable = New Hashtable
        Dim htAcct As Hashtable = New Hashtable

        Dim typesymb As String = ""
        Dim sectype As String = ""
        Dim symbol As String = ""
        Dim mdate As String = ""
        Dim uscore As Integer = 0
        Dim acct As String = ""
        Dim reason As String = ""
        Dim row As Integer = 1
        Dim filename As String = ""
        Dim fullfilename As String = ""

        objDir = New IO.DirectoryInfo(datafilepath)
        objFiles = objDir.GetFiles("*Update*" & curr & ".xlsx")
       
        For Each objFileI In objFiles
            fullfilename = objFileI.FullName
            filename = objFileI.Name
            uscore = filename.IndexOf("_", 6)
            acct = filename.Substring(0, uscore)
            objLOG.WriteLine(acct)

            WkBk = XLSApp.Workbooks.Open(fullfilename)
            WkSht = WkBk.ActiveSheet

            For row = 1 To WkSht.UsedRange.Rows.Count + 2
                If InStr(WkSht.Cells(row, 1).value, "Warning: Missing price") Then

                    typesymb = WkSht.Cells(row, 3).value
                    mdate = WkSht.Cells(row, 5).value

                    If htMiss.ContainsKey(typesymb & mdate) = False Then
                        htMiss.Add(typesymb & mdate, typesymb & mdate)
                        missct += 1
                        sectype = typesymb.Substring(0, 4)
                        symbol = typesymb.Substring(4, Len(typesymb) - 4)
                        reason = "Missing price"
                        misslist &= sectype & vbTab & symbol & vbTab & mdate & vbCrLf
                    End If

                ElseIf InStr(WkSht.Cells(row, 1).value, "Cannot update") Then

                    failurect += 1

                    If WkSht.Cells(row, 4).value = "because of a date gap." Then
                        mdate = WkSht.Cells(row + 1, 4).value
                        reason = "Date gap"
                    ElseIf WkSht.Cells(row, 4).value = "to " Then
                        mdate = WkSht.Cells(row, 5).value
                        reason = WkSht.Cells(row, 6).value
                    Else 'If WkSht.Cells(row, 5).value = "to" Then
                        mdate = WkSht.Cells(row, 6).value
                        reason = WkSht.Cells(row, 7).value
                    End If

                    acct = WkSht.Cells(5, 4).value
                    failurelist &= acct & ": " & mdate & " - " & reason & vbCrLf
                    row += 1

                Else

                    If htAcct.ContainsKey(acct) = False Then
                        htAcct.Add(acct, acct)
                        successlist &= acct & vbCrLf
                    End If

                End If
            Next

            WkSht = Nothing
            WkBk.Save()
            WkBk.Close()

        Next


        If missct > 0 Then
            Form1.txtProgress.Text &= "There are missing prices that must be updated!" & vbCrLf
            Form1.txtProgress.Text &= misslist & vbCrLf
            Form1.ScrollToBottom()
        End If

        If failurect > 0 Then
            Form1.txtProgress.Text &= "Performance could not be updated:" & vbCrLf
            Form1.txtProgress.Text &= failurelist & vbCrLf
            Form1.ScrollToBottom()
        End If

        If missct = 0 And failurect = 0 Then
            Form1.txtProgress.Text &= vbCrLf & curr & " performance updated through " & rundate & "." & vbCrLf & successlist
            Form1.ScrollToBottom()
        End If

        WkBk = Nothing
        XLSApp.Quit()
        XLSApp = Nothing

    End Sub

    Public Function GetEDLM(ByVal edate As Date) As Date
        Dim edlm As Date
        Dim lastmonth As Integer
        Dim lastday As Integer
        Dim lastyear As Integer
        Dim thismonth As Integer = DatePart(DateInterval.Month, edate)

        Select Case thismonth
            Case 1
                lastmonth = 12
                lastday = 31
                lastyear = DatePart(DateInterval.Year, Today()) - 1
            Case Else
                lastmonth = thismonth - 1
                lastyear = DatePart(DateInterval.Year, Today())
        End Select

        Select Case lastmonth
            Case 1, 3, 5, 7, 8, 10, 12
                lastday = 31
            Case 4, 6, 9, 11
                lastday = 30
            Case 2
                Select Case lastyear
                    Case 2008, 2012, 2016, 2020
                        lastday = 29
                    Case Else
                        lastday = 28
                End Select
        End Select

        edlm = Convert.ToDateTime(lastmonth & "/" & lastday & "/" & lastyear)
        Return edlm
    End Function

    Public Function GetBDTM(ByVal sdate As Date) As Date
        Dim bdtm As Date
        bdtm = Convert.ToDateTime(DatePart(DateInterval.Month, Today()) & "/1/" & DatePart(DateInterval.Year, Today()))
        Return bdtm
    End Function

    Public Sub DisplayLogResults()
        Dim objTR As IO.TextReader
        objTR = IO.File.OpenText(updLog)

        While objTR.Peek <> -1
            Form1.txtProgress.Text &= objTR.ReadLine() & vbCrLf
        End While
        objTR.Close()
    End Sub
End Class