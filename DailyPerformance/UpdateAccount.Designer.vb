<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdateAccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.lbxUpdAccts = New System.Windows.Forms.ListBox
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lbxPerfGrps = New System.Windows.Forms.ListBox
        Me.lblAcct = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(12, 331)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(221, 20)
        Me.dtpFrom.TabIndex = 1
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(240, 249)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(205, 32)
        Me.btnUpdate.TabIndex = 2
        Me.btnUpdate.Text = "Update Performance"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'lbxUpdAccts
        '
        Me.lbxUpdAccts.FormattingEnabled = True
        Me.lbxUpdAccts.Location = New System.Drawing.Point(240, 32)
        Me.lbxUpdAccts.Name = "lbxUpdAccts"
        Me.lbxUpdAccts.ScrollAlwaysVisible = True
        Me.lbxUpdAccts.Size = New System.Drawing.Size(205, 199)
        Me.lbxUpdAccts.TabIndex = 4
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(249, 331)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(221, 20)
        Me.dtpTo.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 315)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Update From"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(249, 313)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Update To"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.lbxPerfGrps)
        Me.GroupBox1.Controls.Add(Me.lblAcct)
        Me.GroupBox1.Controls.Add(Me.lbxUpdAccts)
        Me.GroupBox1.Controls.Add(Me.btnUpdate)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(458, 291)
        Me.GroupBox1.TabIndex = 113
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Update Performance"
        '
        'lbxPerfGrps
        '
        Me.lbxPerfGrps.FormattingEnabled = True
        Me.lbxPerfGrps.Location = New System.Drawing.Point(16, 32)
        Me.lbxPerfGrps.Name = "lbxPerfGrps"
        Me.lbxPerfGrps.ScrollAlwaysVisible = True
        Me.lbxPerfGrps.Size = New System.Drawing.Size(205, 199)
        Me.lbxPerfGrps.TabIndex = 114
        '
        'lblAcct
        '
        Me.lblAcct.Location = New System.Drawing.Point(53, 249)
        Me.lblAcct.Name = "lblAcct"
        Me.lblAcct.Size = New System.Drawing.Size(130, 32)
        Me.lblAcct.TabIndex = 112
        Me.lblAcct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'UpdateAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(483, 367)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpTo)
        Me.Controls.Add(Me.dtpFrom)
        Me.Name = "UpdateAccount"
        Me.Text = "Update Accounts"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents lbxUpdAccts As System.Windows.Forms.ListBox
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAcct As System.Windows.Forms.Label
    Friend WithEvents lbxPerfGrps As System.Windows.Forms.ListBox
End Class
