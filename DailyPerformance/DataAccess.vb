Public Class DataAccess
    'Dim datafilepath As String = "M:\Web Collaborations\DailyPerformance\zUpdateFiles\DataFiles\"
    'Dim batpath As String = "M:\Temporary Data Holding\Jesica\VBApplications\BatchFiles\"
    'Dim batfile As String = "DailyPerformance.bat"
    Dim datafilepath As String = "c:\temp\dailyperf\DataFiles\"
    Dim batpath As String = "c:\temp\dailyperf\BatchFiles\"
    Dim batfile As String = "DailyPerformance.bat"

    Public Function GetPRFMGroups(ByVal connPort As String)

        'This function builds a list of performance groups to be updated from APX. This is returned in a datatable to the calling code.

        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_allPRFMgroups", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 50000


        Dim DayOfWeek As String = ""

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtPRFMgrps")

        dt.Columns.Add("Name", GetType(System.String))

        For Each drDSRow In ds.Tables("dtPRFMgrps").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Name") = drDSRow("Name")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function Holidays(ByVal connPort As String) As Data.DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_GetHolidays", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        Dim DayOfWeek As String = ""

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow
        Try
            da.Fill(ds, "dtHolidays")

            dt.Columns.Add("holidaydate", GetType(System.DateTime))

            For Each drDSRow In ds.Tables("dtHolidays").Rows()
                drNewRow = dt.NewRow()
                drNewRow("holidaydate") = drDSRow("holidaydate")
                dt.Rows.Add(drNewRow)
            Next
        Catch ex As System.Exception
            MsgBox(ex.ToString, MsgBoxStyle.Information, "Holidays")
        End Try

        Return dt
    End Function

    Public Function HolidaysWeekends(ByVal connPort As String, ByVal mdate As Date)

        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_GetHolidaysWeekends", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@DATE", mdate)

        Dim DayOfWeek As String = ""

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtHolidayWeekend")

        dt.Columns.Add("Date", GetType(System.DateTime))
        dt.Columns.Add("Day", GetType(System.String))
        dt.Columns.Add("Holiday", GetType(System.String))

        For Each drDSRow In ds.Tables("dtHolidayWeekend").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Date") = drDSRow("Date")
            drNewRow("Day") = drDSRow("Day")
            drNewRow("Holiday") = drDSRow("Holiday")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetMissingIndexPrices(ByVal connPort As String, ByVal rundate As Date) As Data.DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_CheckIndexPrices", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@DATE", rundate)

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtMissDex")

        dt.Columns.Add("IndexName", GetType(System.String))

        For Each drDSRow In ds.Tables("dtMissDex").Rows()
            drNewRow = dt.NewRow()
            drNewRow("IndexName") = drDSRow("IndexName")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetCustomAccounts(ByVal connPort As String) As Data.DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("Select * from PRFM_CustomStart", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        'This is a function that returns a datatable containing perfgroups and perfdates. 
        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtCustom")

        dt.Columns.Add("PerfGroup", GetType(System.String))
        dt.Columns.Add("PerfDate", GetType(System.DateTime))

        For Each drDSRow In ds.Tables("dtCustom").Rows()
            drNewRow = dt.NewRow()
            drNewRow("PerfGroup") = drDSRow("PerfGroup")
            drNewRow("PerfDate") = drDSRow("PerfDate")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetCustomStartDate(ByVal connPort As String, ByVal acct As String) As Date
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_CustomDate", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@ACCT", acct)

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtPerfDate")

        dt.Columns.Add("PerfDate", GetType(System.String))

        For Each drDSRow In ds.Tables("dtPerfDate").Rows()
            drNewRow = dt.NewRow()
            drNewRow("PerfDate") = drDSRow("PerfDate")
            dt.Rows.Add(drNewRow)
        Next

        If dt.Rows.Count = 0 Then
            Return "12/31/2012"
        Else
            Return dt.Rows(0).Item("PerfDate")
        End If
    End Function

    Public Sub AddCustomStartDate(ByVal connPort As String, ByVal acct As String, ByVal stdate As Date)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_AddCustomStartDate", conn) With {
            .CommandType = CommandType.StoredProcedure
        }

        'This acts as a a wrapper for the spFRFM_AddCustomStartDate stored procedure. It lets the user add and 
        'entry into the PRFM_CustomStart table. This allows the performance to start on a schedule different from when the 
        'actual performance starts (usually later).


        cmd.Parameters.AddWithValue("@ACCT", acct)
        cmd.Parameters.AddWithValue("@DATE", stdate)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub DeleteCustomStartDate(ByVal connPort As String, ByVal acct As String)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("Delete from PRFM_PerfUpdate where PerfGroup = '" & acct & "'", conn)

        'Deletes account data from PRFM_PerfUpdate (I can't find this table). 

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub DeleteDisplayList(ByVal connPort As String)
        Dim sql As String = "Delete from PRFM_ListMembers"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub UpdateDisplayList(ByVal connPort As String, ByVal acct As String, ByVal inc As Date, ByVal curr As String, ByVal b1 As String, ByVal b2 As String, ByVal b3 As String, ByVal col As Integer, ByVal listorder As Integer)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_InsertDisplayList", conn) With {
            .CommandType = CommandType.StoredProcedure
        }

        cmd.Parameters.AddWithValue("@Name", acct)
        cmd.Parameters.AddWithValue("@inc", inc)
        cmd.Parameters.AddWithValue("@Curr", curr)
        cmd.Parameters.AddWithValue("@b1", b1)
        cmd.Parameters.AddWithValue("@b2", b2)
        cmd.Parameters.AddWithValue("@b3", b3)
        cmd.Parameters.AddWithValue("@col", col)
        cmd.Parameters.AddWithValue("@listorder", listorder)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub DeletePerformance(ByVal connPort As String, ByVal enddate As Date)
        Dim sql As String = "Delete from PRFM_Performance where PeriodEnd = '" & enddate & "'"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Function PerfDataExists(ByVal connPort As String, ByVal perfdate As Date, ByVal acct As String, ByVal curr As String, ByVal incept As Date)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_AccountDataExists", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@perend", perfdate)
        cmd.Parameters.AddWithValue("@acct", acct)
        cmd.Parameters.AddWithValue("@curr", curr)
        cmd.Parameters.AddWithValue("@incept", incept)
        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim display As String = ""

        conn.Open()
        da.Fill(ds, "dtCheckPerf")
        conn.Close()

        Dim rowct As Integer = ds.Tables(0).Rows.Count

        If rowct > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub ImportPerformance(ByVal connPort As String, ByVal enddate As Date, ByVal acct As String, ByVal curr As String, ByVal per As String, ByVal perf As Decimal, ByVal incdate As Date)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_InsertPerf", conn) With {
            .CommandType = CommandType.StoredProcedure
        }

        cmd.Parameters.AddWithValue("@PeriodEnd", enddate)
        cmd.Parameters.AddWithValue("@Account", acct)
        cmd.Parameters.AddWithValue("@Currency", curr)
        cmd.Parameters.AddWithValue("@Period", per)
        cmd.Parameters.AddWithValue("@Performance", perf)
        cmd.Parameters.AddWithValue("@Inception", incdate)

        'Debug.WriteLine(acct & vbTab & per & vbTab & incdate)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Sub UpdatePerformance(ByVal connPort As String, ByVal rundate As Date, ByVal Aytd As Decimal)
        Dim sql As String = "UPDATE PRFM_Performance " & _
                            "SET Performance = " & Aytd & " " & _
                            "WHERE Account = '130-30 Net' " & _
                            "AND PeriodEnd = '" & rundate & "' " & _
                            "AND Period = 'YTD'"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Public Function GetBenchmarkNames(ByVal connPort As String) As Data.DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("select * from prfm_benchnames", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtBench")

        dt.Columns.Add("ReportName", GetType(System.String))
        dt.Columns.Add("DisplayName", GetType(System.String))

        For Each drDSRow In ds.Tables("dtBench").Rows()
            drNewRow = dt.NewRow()
            drNewRow("ReportName") = drDSRow("ReportName")
            drNewRow("DisplayName") = drDSRow("DisplayName")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetPerformance(ByVal connPort As String, ByVal rdate As Date, ByVal longshort As String) As DataSet
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_PivotPerf", conn) With {
            .CommandType = CommandType.StoredProcedure
        }
        cmd.Parameters.AddWithValue("@date", rdate)
        cmd.Parameters.AddWithValue("@LS", longshort)

        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        Dim ds As New DataSet
        'Dim dt As New DataTable
        Dim display As String = ""

        If longshort = "no" Then
            display = "long only"
        ElseIf longshort = "yes" Then
            display = "short only"
        Else
            display = "all"
        End If

        Try
            conn.Open()
            da.Fill(ds, "dtPerformance")
            conn.Close()
        Catch ex As System.Exception
            MsgBox("Get Performance - failure" & vbCrLf & rdate & ": " & display & " " & ds.Tables.Count & " tables", MsgBoxStyle.Critical, "Unable to retrieve performance")
        End Try

        Return ds
    End Function

    Public Function GetAllPerf(ByVal connPort As String, ByVal rdate As String) As Data.DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_AllPerf", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@date", rdate)

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        Try
            da.Fill(ds, "dtAllPerf")

            dt.Columns.Add("Account", GetType(System.String))
            dt.Columns.Add("Currency", GetType(System.String))
            dt.Columns.Add("DTD", GetType(System.String))
            dt.Columns.Add("MTD", GetType(System.String))
            dt.Columns.Add("QTD", GetType(System.String))
            dt.Columns.Add("YTD", GetType(System.String))

            For Each drDSRow In ds.Tables("dtAllPerf").Rows()
                drNewRow = dt.NewRow()
                drNewRow("Account") = drDSRow("Account")
                drNewRow("Currency") = drDSRow("Currency")
                drNewRow("DTD") = drDSRow("DTD")
                drNewRow("MTD") = drDSRow("MTD")
                drNewRow("QTD") = drDSRow("QTD")
                drNewRow("YTD") = drDSRow("YTD")
                dt.Rows.Add(drNewRow)
            Next
        Catch ex As System.Exception
            MsgBox("Get All Perf - failure", MsgBoxStyle.Critical, "GetAllPerf")
        End Try

        Return dt
    End Function

    Public Function GetAccountsIndices(ByVal connPort As String) As DataSet
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_GetAcctsIndices", conn) With {
            .CommandType = CommandType.StoredProcedure
        }

        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        Dim ds As New DataSet

        Try
            conn.Open()
            da.Fill(ds, "dtLists")
            conn.Close()
        Catch ex As System.Exception
            MsgBox("Get Account/Index Lists" & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Data Error")
        End Try

        Return ds
    End Function

    Public Function GetAcctGrpList(ByVal connPort As String) As Data.DataTable
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("select displayname, name from apxsql15.APXFirm.dbo.aoobject where name like 'prfm%'", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtPRFMgrps")

        dt.Columns.Add("DisplayName", GetType(System.String))
        dt.Columns.Add("Name", GetType(System.String))

        For Each drDSRow In ds.Tables("dtPRFMgrps").Rows()
            drNewRow = dt.NewRow()
            drNewRow("DisplayName") = drDSRow("DisplayName")
            drNewRow("Name") = drDSRow("Name")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Function GetLastDivDate(ByVal connPort As String, ByVal rundate As Date) As Date
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPort)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spPRFM_ImportDividendDate", conn)
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@date", rundate)

        Dim ds As New DataSet
        Dim dt As New Data.DataTable
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow

        da.Fill(ds, "dtDivDate")

        dt.Columns.Add("LastDivDate", GetType(System.String))

        For Each drDSRow In ds.Tables("dtDivDate").Rows()
            drNewRow = dt.NewRow()
            drNewRow("LastDivDate") = drDSRow("LastDivDate")
            dt.Rows.Add(drNewRow)
        Next

        Return dt.Rows(0).Item("LastDivDate")
    End Function


End Class
